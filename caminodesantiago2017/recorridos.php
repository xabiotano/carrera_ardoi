<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>XXIV CARRERA CAMINO DE SANTIAGO -- EDICIÓN 2017 5/10KM</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/noinscripcion.css" rel="stylesheet">
	<link href="css/animate.css" rel="stylesheet">	
	<link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
	<style>
		body{
		
		background-color:#19153d;
		}
	</style>    
</head><!--/head-->

<body>
	<header id="header" role="banner">		
		<div class="main-nav">
			<div class="container">
				
		        <div class="row">	        		
		            <div class="navbar-header">
		                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		                    <span class="sr-only">Toggle navigation</span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
		                <a class="navbar-brand" href="index.php">
		                 <img  src="images/logo.png"/>
		                  </a>                    
		            </div>
		             <?php include_once 'componentes/navbar.php';?>
		        </div>
	        </div>
        </div>                    
    </header>
    <!--/#header--> 

    

		
	
	
    <div class="container">
      <!-- Example row of columns -->
      <br/>
	        <br/>
	        <br/>
	        <br/>
	        <br/>
	        <br/>
	        <br/>
	        <br/>
      <div class="row">
      	<div class="col-md-12">
	          <h2>Mapa del recorrido</h2>
	          <p>Se puede observar el recorrido por todo el pueblo de Zizur Mayor durante el transcurso de la carrera. Los corredores de 5km lo haran una sola vez, los de 10km dos veces </p>
	         <div class="col-sm-8 col-md-12">
					    <div class="thumbnail">
					      <img src="images/circuito.jpg" alt="Mapa de la ruta">
					      <div class="caption">
					        <h3>Recorrido por zizur</h3>
					        <p>Los corredores de 5km daran una vuelta, los de 10km daran dos.</p>
					        
					      </div>
					    </div>
				  	</div>
				  <br/>
	         
	       </div>
        
        </div>
        <div class="row">
	        <div class="col-md-6">
	          <h2>Perfil de 1 vuelta</h2>
	          <p>Este año el recorrido incluye alguna cuesta. Por lo que el perfil te resultará de interés</p>
	          <div class="col-sm-8 col-md-12">
					    <div class="thumbnail">
					      <img src="images/perfil.png" alt="Perfil de la ruta">
					      <div class="caption">
					        <h3>Altimetría</h3>
					        <p>Recuerda que lo haras una vez si corres 5km , y dos veces  si corres 10km</p>
					        
					      </div>
					    </div>
				  </div>
				  <br/>
	         
	        </div>
        </div>
       <div class="row">
	        <div class="col-md-6">
	        	<div class="thumbnail">
	        	
					    
					      <div class="caption">
					        <h3>WikiLoc</h3>
					          <p>¿Quieres navegar por el recorrido?Adelante!</p>
					        <iframe frameBorder="0" src="http://es.wikiloc.com/wikiloc/spatialArtifacts.do?event=view&id=12814614&measures=off&title=off&near=off&images=off&maptype=S" width="500" height="400"></iframe><div style="background-color:#fff;color:#777;#font-size:11px;line-height:12px;">Powered by <a style="color:#06d;#font-size:11px;line-height:12px;" target="_blank" href="http://es.wikiloc.com">Wikiloc</a></div>
					       
					      </div>
			
				</div>
	        </div>
        </div>
        
        
        

      <hr>

     
    </div> <!-- /container -->

	





	
	
    <!--/#contact-->

    <?php include_once 'componentes/footer.php';?>
  
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
  	<script type="text/javascript" src="js/gmaps.js"></script>
	<script type="text/javascript" src="js/smoothscroll.js"></script>
    <script type="text/javascript" src="js/jquery.parallax.js"></script>
    <script type="text/javascript" src="js/coundown-timer.js"></script>
    <script type="text/javascript" src="js/jquery.scrollTo.js"></script>
    <script type="text/javascript" src="js/jquery.nav.js"></script>
    <script type="text/javascript" src="js/main.js"></script>  
</body>
</html>