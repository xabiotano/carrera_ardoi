
<?php 
require_once 'inscripcion/classes/Sponsors.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Carrera Camino de Santiago Club Ardoi">
    <meta name="author" content="Club Atletismo Ardoi">
    
    
    
    <title>XXIV CARRERA CAMINO DE SANTIAGO - 2017 5/10KM</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/noinscripcion.css" rel="stylesheet">
	<link href="css/animate.css" rel="stylesheet">	
	<link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<header id="header" role="banner">		
		<div class="main-nav">
			<div class="container">
				
		        <div class="row">	        		
		            <div class="navbar-header">
		                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		                    <span class="sr-only">Toggle navigation</span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
		               <!--  <a class="navbar-brand" href="index.html">
		                	<img class="img-responsive" src="images/logo.png" alt="logo" id="logo_ardoi" style="width:90px;height:90px;margin-top:30px;">
		                </a> -->
		                 <a class="navbar-brand" rel="home" href="#" title="Buy Sell Rent Everyting">
					        <img  src="images/logo.png"/>
					    </a>                   
		            </div>
		            <?php include_once 'componentes/navbar.php';?>
		        </div>
	        </div>
        </div>                    
    </header>
    <!--/#header--> 

    <section id="home">	
		<div id="main-slider" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#main-slider" data-slide-to="0" class="active"></li>
				<li data-target="#main-slider" data-slide-to="1"></li>
				<li data-target="#main-slider" data-slide-to="2"></li>
			</ol>
			<div class="carousel-inner">
				<div class="item active">
					<img class="img-responsive" src="images/slider/bg2.jpg" alt="slider">	
					<div class="carousel-caption">
						<h2>XXIV Carrera Camino de Santiago 2017</h2>
						<h4>5Km | 10km | Categorias menores</h4>
						<a href="informacion.php">Domingo 2 de Abril  <i class="fa fa-angle-right"></i></a>
					</div>
				</div>
				<div class="item">
					<img class="img-responsive" src="images/slider/bg1.jpg" alt="slider">						
					<div class="carousel-caption">
						<h2>XXIV Carrera Camino de Santiago 2017</h2>
						<h4>5Km | 10km | Categorias menores</h4>
						<a href="informacion.php">Domingo 2 de Abril  <i class="fa fa-angle-right"></i></a>
					</div>
				</div>
				<div class="item">
					<img class="img-responsive" src="images/slider/bg3.jpg" alt="slider">	
					<div class="carousel-caption">
						<h2>XXIV Carrera Camino de Santiago 2017</h2>
						<h4>5Km | 10km | Categorias menores</h4>
						<a href="informacion.php">Domingo 2 de Abril  <i class="fa fa-angle-right"></i></a>
					</div>
				</div>				
			</div>
		</div>    	
    </section>
	<!--/#home-->

		<section id="about">
			<div class="guitar2">				
				<img class="img-responsive" src="images/guitar2.jpg" alt="guitar">
			</div>
			<div class="col-md-10" style="position:relative; right:10px;">					
						<h2>Carrera</h2>
						<p align="justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Un año más tenemos la suerte de ofrecer la nueva edición de la Carrera 

de Zizur Mayor, Camino de Santiago. La cita tendrá lugar el próximo 2 

de abril en el mismo circuito que el año pasado gustó a tanta gente. 

El objetivo es que éste año el recorrido nos enseñe 

rincones de nuestro pueblo, por eso, discurre por la parte más bonita de 

Zizur Mayor, llegando hasta la zona de Ardoi, y atravesando una parte 

del Camino de Santiago, ofreciendo a los participantes la posibilidad de 

disfrutar del entorno haciendo lo que más nos gusta, correr! <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;En ésta 

edición queremos que todos sean protagonistas y por eso tendremos las 

carreras infantiles para los mas pequeños, y la posibilidad de correr 5 o 

10 km en la carrera absoluta. La carrera de 10 km, permitirá a los atletas 

puntuar para la quinta edición del Circuito Navarro de Running. 

<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;El Club Ardoi y sus colaboradores queremos hacer de ésta carrera un 

camino para el disfrute de todos, tanto los que corren como los que 

animan, y esperamos que todos vengan a pasar un buen rato con 

nosotros. 

Además, éste año, con el fin de mejorar la atención al atleta, se harán 

todas las inscripciones con antelación y el día de la carrera bastará con 

que recojas tu dorsal y tu camiseta junto con tu bolsa del corredor!

<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nos vemos el Domingo 2 de Abril en la fiesta del atletismo de Zizur 

Mayor ¿A qué esperas para inscribirte?</p>
						<a href="informacion.php" class="btn btn-primary">Mas información <i class="fa fa-angle-right"></i></a>

			</div>
		</section><!--/#about-->

	<section id="event">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div id="event-carousel" class="carousel slide" data-interval="false">
						<h2 class="heading">LA CARRERA</h2>
						<a class="even-control-left" href="#event-carousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
						<a class="even-control-right" href="#event-carousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
						<div class="carousel-inner">
							<div class="item active">
								<div class="row">
									<div class="col-sm-3">
										<div class="single-event">
										<a target="_blank" href='descargas/cartelccs2016.jpg'>
											<img class="img-responsive" src="descargas/cartelccs2016.jpg" alt="event-image">
											</a>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="single-event">
											<img class="img-responsive" src="images/event/event1.jpg" alt="event-image">
											<h4>En Zizur Mayor (Navarra)</h4>
											<h5>a 5 min de Pamplona</h5>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="single-event">
											<img class="img-responsive" src="images/event/event2.jpg" alt="event-image">
											<h4>Para mayores y pequeños!</h4>
											<h5>Categoría menores y absolutas</h5>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="single-event">
											<img class="img-responsive" src="images/event/event3.jpg" alt="event-image">
											<h4>Bolsa del corredor</h4>
											<h5>Para todos los participantes de 5 & 10Km</h5>
										</div>
									</div>
									
									
								</div>
							</div>
							<div class="item">
								<div class="row">
									<div class="col-sm-4">
										<div class="single-event">
											<img class="img-responsive" src="images/event/event1.jpg" alt="event-image">
											<h4>Chester Bennington</h4>
											<h5>Vocal</h5>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="single-event">
											<img class="img-responsive" src="images/event/event2.jpg" alt="event-image">
											<h4>Mike Shinoda</h4>
											<h5>vocals, rhythm guitar</h5>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="single-event">
											<img class="img-responsive" src="images/event/event3.jpg" alt="event-image">
											<h4>Rob Bourdon</h4>
											<h5>drums</h5>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="guitar">
					<img class="img-responsive" src="" alt="">
				</div>
			</div>			
		</div>
	</section><!--/#event-->





	
	
	<section id="explore">
		<div class="container">
			<div class="row">
				<div class="watch">
					<img class="img-responsive" src="images/watch.png" alt="">
				</div>				
				<div class="col-md-4 col-md-offset-2 col-sm-5">
					<h2>Será en</h2>
				</div>				
				<div class="col-sm-7 col-md-6">					
					<ul id="countdown">
						<li>					
							<span class="days time-font">00</span>
							<p>días </p>
						</li>
						<li>
							<span class="hours time-font">00</span>
							<p class="">horas </p>
						</li>
						<li>
							<span class="minutes time-font">00</span>
							<p class="">minutos</p>
						</li>
						<li>
							<span class="seconds time-font">00</span>
							<p class="">segundos</p>
						</li>				
					</ul>
				</div>
			</div>
			<div class="cart">
				<a href="inscripcion/elegirCarrera.php"><i class="fa fa-shopping-cart"></i> <span>INSCRIBETE AHORA</span></a>
			</div>
		</div>
	</section><!--/#explore-->
	
	
	<section id="sponsor">
		<div id="sponsor-carousel" class="carousel slide" data-interval="false">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h2>Patrocinadores</h2>			
						<a class="sponsor-control-left" href="#sponsor-carousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
						<a class="sponsor-control-right" href="#sponsor-carousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
						<div class="carousel-inner">
							<div class="item active">
						
								<ul>
								<?php 
								$sponsors=Sponsors::GetPatrocinadores();
								foreach ($sponsors as $k => $sponsor) {?>
																					
							
									<li><a href="<?php echo $sponsor->ruta_web?>" target="_blank"><img style="margin:0 auto;max-width:200px; border:#000 solid 1px;" class="img-responsive" title="<?php echo "Visita la Página Web de ".$sponsor->patrocinador;?>" src="http://ardoi.es/CCSantiago2016/pagina/patrocinadores/<?php echo $sponsor->ruta_logo;?>" alt=""></a></li>
										
								<?php 
								}
								?>
								
							
									<!--  <li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor1.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor2.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor3.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor4.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor5.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor6.png" alt=""></a></li>-->
								</ul>
							</div>
							<div class="item">
								<ul>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor6.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor5.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor4.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor3.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor2.png" alt=""></a></li>
									<li><a href="#"><img class="img-responsive" src="images/sponsor/sponsor1.png" alt=""></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>				
			</div>
			
		</div>
	</section><!--/#sponsor-->
	
	
	</section>
    <?php include_once 'componentes/footer.php';?>

  
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
  	<script type="text/javascript" src="js/gmaps.js"></script>
	<script type="text/javascript" src="js/smoothscroll.js"></script>
    <script type="text/javascript" src="js/jquery.parallax.js"></script>
    <script type="text/javascript" src="js/coundown-timer.js"></script>
    <script type="text/javascript" src="js/jquery.scrollTo.js"></script>
    <script type="text/javascript" src="js/jquery.nav.js"></script>
    <script type="text/javascript" src="js/main.js"></script>  
</body>
</html>