<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
   <title>XXIV CARRERA CAMINO DE SANTIAGO -- EDICIÓN 2017 5/10KM</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/noinscripcion.css" rel="stylesheet">
	<link href="css/animate.css" rel="stylesheet">	
	<link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
	<style>

		body{
			
			background-color:#19153d;
			}
	</style>


</head><!--/head-->

<body>
	<header id="header" role="banner">		
		<div class="main-nav">
			<div class="container">
			  
		        <div class="row">	        		
		            <div class="navbar-header">
		                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		                    <span class="sr-only">Toggle navigation</span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
		                <a class="navbar-brand" href="index.php">
		                 <img  src="images/logo.png"/>	
		                 </a>                    
		            </div>
		             <?php include_once 'componentes/navbar.php';?>
		        </div>
	        </div>
        </div>                    
    </header>
    <!--/#header--> 

   
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-md-10">
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				
				<br/>
					<h1><span class="label label-primary">Reglamento de la carrera</span></h1>	
					
  
							
							<h1>XXIV Carrera Popular Camino de Santiago – Donejakue Bidea XXIV. Herri Lasterketa Reglamento</h1>
							<h2>Inscripciones / Izen emateak :</h2>
									
							
							• Hasta el 31 de Marzo en la página carrera.ardoi.es<br/>
							• Desde el 27 al 31 de Marzo en las oficinas del Club Atletismo Ardoi, ubicadas en el
							Polideportivo de Zizur Mayor: Paseo Kirolaldea no 2, de 17 a 19:30 h.<br/>
							• Teléfono de la organización: 948 18 19 91<br/>
							• No se realizará ninguna inscripción el día de la carrera.<br/>
							• Al hacer la inscripción deberá indicarse en cuál de las dos carreras se desea participar. Los
							dorsales de ambas carreras, 5 y 10 Km son diferentes.<br/>
							• Para participar en la prueba de 10 Km. la cuota de inscripción es de 10 € mientras que para
							la de 5 Km. es de 8 €.<br/>
							• En la prueba absoluta, sólo la carrera de 10 Km. puntuará para la V edición del Circuito
							Navarro de Running / Nafar Zirkuitua 2017.<br/>
							• Todo aquél participante que haya efectuado su inscripción vía internet deberá recoger su
							dorsal en las mesas que la organización dispondrá al efecto en el frontón mediano del Polideportivo de Zizur, junto a la línea de salida. Para ello habrá mesas diferentes para recoger los dorsales de las pruebas infantiles, de la carrera de 5 Km y de la carrera de 10 Km.<br/>
							• Para la recogida del dorsal es imprescindible la presentación del justificante de inscripción impreso que le será facilitado en el momento del pago en el que consta el nombre y el número de dorsal del atleta.<br/>
							• Recorrido no homologado.<br/>
							• Para las carreras infantiles, desde prebenjamines hasta juveniles inclusive, la “cuota de
							participación” será la aportación de al menos 1 Kg de alimentos no perecederos a entregar
							previamente a la salida de cada una de esta pruebas, junto a la misma línea de salida.<br/>
							• Estos alimentos serán donados a la Asociación Navarra de Amigos del Sáhara (A.N.A.S.)
							para los campamentos de refugiados saharauis en Tinduf (Argelia).<br/>
							<h2>Premios / Sariak :</h2>
							
							
							• Bolsa obsequio a todos los corredores que lleguen, con su dorsal, a meta.<br/>
							• Trofeo a los tres primeros de cada categoría masculina y femenina en las ocho primeras carreras ( carreras infantiles). La entrega se hará a partir de las 11 de la mañana, al término de la carrera
							de Cadetes y Adaptados.<br/>
							• Carreras absolutas:<br/>
							- Juveniles.- Trofeo a las tres primeras chicas y tres primeros chicos.<br/>
							- Carrera de 5 Km.- Premio a los tres primeros y primeras, categoría absoluta. - Carrera de 10 Km.- Premio a los tres primeros y primeras en las categorías:
							Absoluta, Veteranos y Locales Absoluta ( censados en Zizur Mayor )<br/>
							- La categoría Absoluta comprende las categorías Junior, Promesa, Senior y
							Veteranos. Asimismo la categoría de Veteranos se compondrá de todos aquéllos corredores que el día de la prueba tengan cumplidos 35 años o más.<br/>
							<h2>Reglamento / Araudiak :</h2>
							
							
							• El Club de Atletismo Ardoi, con la ayuda de empresas patrocinadoras y colaboradoras, organiza la XXIV edición de la Carrera Popular Camino de Santiago en la que podrán participar todas las personas que así lo deseen.<br/>
							• La Organización recomienda a las corredoras y corredores realizar un reconocimiento médico previo.<br/>
							• La Organización no se responsabiliza de quienes participen sin tener la edad reglamentaria a su categoría.<br/>
							• Quien corra sin haber realizado la inscripción lo hará bajo su única y exclusiva responsabilidad.<br/>
							• Los dorsales se colocarán visiblemente en el pecho sin doblar ni romper.<br/>
							• Se publicarán los resultados en los aledaños de la línea de salida. Además, la misma tarde de la
							    carrera los resultados definitivos serán accesibles desde la página web http://www.ardoi.es en
							 
							ellos se podrán además consultar los resultados en función de las categorías estipuladas en la V
							edición del Circuito Navarro de Running – Nafar Zirkuitua 2017.<br/>
							• Las posibles reclamaciones deberán ser presentadas ante la organización en un plazo máximo de
							30’ después de la publicación de los resultados.<br/>
							• Habrá servicio de vestuarios y duchas a disposición de todo participante en las instalaciones del
							Polideportivo de Zizur Mayor.<br/>
							• Habrá un punto de avituallamiento líquido (agua) al paso por la plaza de la Mujer. Km 3 y 8 aprox.<br/>
							• El recorrido será controlado por la Policía Municipal de Zizur Mayor y colaboradores del Club
							Atletismo Ardoi.<br/>
							• La Organización se reserva el derecho a practicar pruebas antidoping a cualquiera de los atletas
							participantes a fin de salvaguardar y defender el juego limpio en la práctica deportiva. Así como a
							descalificar a aquél atleta que mantenga una actitud claramente antideportiva.<br/>
							• La Organización declina las responsabilidad por daños, perjuicios o lesiones que la participación de un atleta en esta prueba pueda ocasionar a sí mismo, a otra persona, o incluso en el caso de
							que terceras personas puedan causar al atleta.<br/>
							• Las Organización tendrá contratado un seguro de responsabilidad civil. Asimismo la prueba
							contará con servicio de ambulancia y personal cualificado para atender los posibles incidentes
							derivados del desarrollo de las pruebas.<br/>
							• Todo participante, por el hecho de serlo, acepta el presente Reglamento.<br/>
							<br/>
							<br/>
							<br/>
							
					<div class="col-xs-6 col-md-2">
				    <a href="http://www.ardoi.es/CCSantiago2017/pagina/reglamento2017.pdf" class="thumbnail">
				      <img src="images/pdfion.jpg" alt="...">
				     <p>Version completa en PDF</p>
				    </a>
				   
				 </div>
				</div>
			</div>
			</div>

    <?php include_once 'componentes/footer.php';?>
  
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
  	<script type="text/javascript" src="js/gmaps.js"></script>
	<script type="text/javascript" src="js/smoothscroll.js"></script>
    <script type="text/javascript" src="js/jquery.parallax.js"></script>
    <script type="text/javascript" src="js/coundown-timer.js"></script>
    <script type="text/javascript" src="js/jquery.scrollTo.js"></script>
    <script type="text/javascript" src="js/jquery.nav.js"></script>
    <script type="text/javascript" src="js/main.js"></script>  
</body>
</html>