<?php 
	require_once 'inscripcion/classes/Carrera.php';
	require_once 'inscripcion/classes/ConfigCarrera.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>CARRERA CAMINO DE SANTIAGO 2017 5/10KM</title>
     <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/noinscripcion.css" rel="stylesheet">
	<link href="css/animate.css" rel="stylesheet">	
	<link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <style>
		body{
			background-color:#19153d;
			color:#000;
		}
	</style> 
</head><!--/head-->

<body>
	<header id="header" role="banner">		
		<div class="main-nav">
			<div class="container">
				    
		        <div class="row">	        		
		            <div class="navbar-header">
		                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		                    <span class="sr-only">Toggle navigation</span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
		                <a class="navbar-brand" href="index.html">
		                	 <img  src="images/logo.png"/>
		                </a>                    
		            </div>
		             <?php 
		            	include_once 'componentes/navbar.php';
		            ?>
		        </div>
	        </div>
        </div>                    
    </header>
    <!--/#header--> 

    
 <br/>
      <br/>
      <br/>
      <br/>
      <br/>
      <br/>
      <br/>
	<div class="container">
      
	     <div class="row">
	      	<div class="col-md-12">
	      	      <br/>
     
     glyphicon glyphicon-save-file
     
	      	<div class="panel panel-default">
	      		<div class="panel-heading">
			   		 <h3 class="panel-title">Tríptico &nbsp;&nbsp;<span class="glyphicon glyphicon-info-sign"></span></h3>
			  	</div>
	 		  	<div class="panel-body">
	 		   <a href='descargas/triptico.pdf' type="button" target="_blank" class="btn btn-default">Ver Tríptico</a>
	 		   <a href='descargas/cartelccs2016.jpg' type="button" target="_blank" class="btn btn-default">Ver Cartel</a>
					 
				 
	 		  	</div>
	 		 </div>
	 		</div>
 		  		
 		</div>
 		


      <div class="row">
	      	<div class="col-md-12">
	      	      <br/>
     
    
     
	      	<div class="panel panel-default">
	      		<div class="panel-heading">
			   		 <h3 class="panel-title">Inscripciones &nbsp;&nbsp;<span class="glyphicon glyphicon-pencil"></span></h3>
			  	</div>
	 		  	<div class="panel-body">
	 		  	<p>Abiertas hasta el
	 		  	 <?php 
	 		  		$carrera=new Carrera(ConfigCarrera::ID_CARRERA);
	 		  		echo $carrera->ultimodiainscripcion->format('d')." de Marzo";
	 		  	?></p> 
	 		  	<p>No se realizara ninguna inscripcion el día de la carrera</p>
	 		  	<p>Télefono de la organización: 948 181991 de 17 a 19h.</p>
	 		  	<p>  Para inscribirte, deberás elegir tu categoría. Si naciste en el año 2001 ó antes podras apuntarte a las pruebas de 5Km &10Km. Clica en "Categorías menores" si tu fecha de nacimiento es posterior al 1 de Enero de 2002.
				   La cuota de inscripción previa es de 8/10€(dependiendo de la distancia) para los mayores. Para las categorías hasta Juvenil, un kilo de alimentos no perecederos.   El día de la prueba no se hará ninguna inscripción
				 </p>
				  <table class="table-responsive table-bordered ">
			    <thead>
			      <tr class="active">
			        
			         <th>Categoría</th>
			        <th>Distancias Infantiles</th>
			        <th>5km</th>
			        <th>10km</th>
			      </tr>
			    </thead>
			    <tbody>
			      <tr>
			        <td>Hasta Juveniles</td>
			        <td class="success" >Alimentos perecederos</td>
			        <td class="success">Alimentos perecederos (Juveniles)</td>
			        <td class="danger">--</td>
			      </tr>
			      <tr>
			        <td>Resto categorías</td>
			        <td class="danger">--</td>
			        <td class="success">8€</td>
			        <td class="success">10€</td>
			      </tr>
			     
			    </tbody>
			  </table>
			
				 
	 		  	</div>
	 		 </div>
	 		</div>
 		  		
 		</div>
 		
 		<div class="row">
	      	<div class="col-md-12">
	      	      <br/>
     
	      	<div class="panel panel-default">
	      		<div class="panel-heading">
			   		 <h3 class="panel-title">Premios &nbsp;&nbsp;<span class="glyphicon glyphicon-gift"></span> </h3>
			  	</div>
	 		  	<div class="panel-body">
	 		  	<ul>
	 		  		<li><b>En la Carreras Infantiles :</b>
		 		  		<ul>
		 		  			<li>Trofeos a los 3 primeros clasificados en las categorías menores, tanto chicos como chicas.</li>
			 	  		</ul>
		 		 	 </li>
		 		  	<li><b>En la Carreras Absolutas :</b>
		 		  		<ul>
		 		  			<li>Para el primer clasificada/a: Txapela + Estuche 3 Botellas de vino (P.de Viana) + Colonia Equivalenza + Ramo de flores</li>
		 		  			<li>Segundo/a clasificado/a: Trofeo + Colonia Equivalenza + Ramo de flores</li>
							<li>Tercer/a clasificado/a: Trofeo + Colonia Equivalenza + Ramo de flores</li>
							<li>Todo ello para las categorías:Absoluta M/F en la carrera de 5Km y Absoluta M/F, Veteranos M/F y Local Absoluta M/F en la carrera de 10Km</li> 
							<li>Además para el mejor y la mejor de la carrera de 10Km un bono de Electroterapia.</li>
						
						
						</ul>
						</li>
						<li><b>Para todos los que lleguen con el dorsal a meta :</b>
							<ul>
								<li>Camiseta técnica.</li>
			 		  			<li>Bolsa del corredor.</li>
			 		  			<!--  <li>Al finalizar la carrera, masajes gratis impartidos por la escuela superior Surya.</li>-->
			 		  			<li>En la carrera absoluta de 10 Km. se repartirán los puntos valederos para la clasificación del <a target="_blank" href="http://cnrunning.com"> V Circuito Navarro de Running</a></li>
		 		  			</ul>
		 		  		</li>
		 		</ul>
	 		  	

 	


 	 	




	 		  	</div>
	 		 </div>
	 		</div>
 		  		
 		</div>
 		
 		
 			

 		
      
      <div class="row">
	      	<div class="col-md-12">
	      	
	      	<div class="panel panel-default">
	      		<div class="panel-heading">
			   		 <h3 class="panel-title">Entrega de dorsales&nbsp;&nbsp;<span class="glyphicon glyphicon-envelope"></span> </h3>
			  	</div>
	 		  	<div class="panel-body">
	 		  	<p>La entrega de dorsales y de la camiseta 
	 		  	se entregaran el domingo 2 de Abril (el mismo dia de la carrera) , en el fronton mediano del polideportivo de Zizur Mayor
	 		  	</div>
	 		 </div>
	 		</div>
 		  		
 		</div>
 		
      
      <div class="row">
      	<div class="col-md-12 col-sm-12">
      	
      	<div class="panel panel-default col-md-12 ">
      		<div class="panel-heading">
		   		 <h3 class="panel-title">Horarios&nbsp; &nbsp; <span class="glyphicon glyphicon-time"></span></h3>
		  	</div>
 		  	<div class="panel-body">
 		  		
 		  		<table class="table table-bordered table-hover">
			    <thead>
			      <tr class="active">
			        <th>HORA/ORDUA</th>
			        <th>LASTERKETA/CARRERA</th>
			        <th>LUZERA/DISTANCIA</th>
			        <th>JAIOTZE URTEA/AÑO NACIMIENTO</th>
			      </tr>
			    </thead>
			    <tbody>
			      <tr class="active">
			        <td>10:00</td>
			        <td>PREBENJAMIN MASC.</td>
			        <td>400m</td>
			        <td>A PARTIR DEL 2010</td>
			      </tr>
			      <tr class="active">
			        <td>10:05</td>
			        <td>PREBENJAMIN FEM.</td>
			        <td>400m</td>
			         <td>A PARTIR DEL 2010</td>
			      </tr>
			      <tr class="active">
			        <td>10:10</td>
			        <td>BENJAMIN MASC.</td>
			        <td>800m</td>
			         <td>2008-2009</td>
			      </tr>
			      <tr class="active">
			        <td>10:15</td>
			        <td>BENJAMIN FEM.</td>
			        <td>800m</td>
			         <td>2008-2009</td>
			      </tr>
			      <tr class="active">
			        <td>10:20</td>
			        <td>ALEVIN MASC.</td>
			        <td>1000m</td>
			         <td>2006-2007</td>
			      </tr>
			      <tr class="active">
			        <td>10:25</td>
			        <td>ALEVIN FEM.</td>
			        <td>1000m</td>
			         <td>2006-2007</td>
			      </tr>
			      <tr class="active">
			        <td>10:40</td>
			        <td>INFANTIL M/F.</td>
			        <td>1340m</td>
			         <td>2004-2005</td>
			      </tr>
			      <tr class="active">
			        <td>10:50</td>
			        <td>CADETES/ADAPTADOS.</td>
			        <td>1600m</td>
			         <td>2002-2003</td>
			      </tr>
			      <tr class="warning">
			        <td>11:00</td>
			        <td>ACADEMIA DE BAILE EVA ESPUELAS.</td>
			        <td></td>
			         <td></td>
			      </tr>
			      <tr class="warning">
			        <td>11:10</td>
			        <td>ENTREGA DE PREMIOS CARRERAS EFECTUADAS.</td>
			        <td></td>
			         <td></td>
			      </tr>
			      
			    </tbody>
			    
			  </table>
			  <h1>CARRERA ABSOLUTA/LASTERKETA ABSOLUTUA</h1>
			  
			  <table class="table table-bordered">
			    <thead>
			      <tr class="active">
			        <th>HORA/ORDUA</th>
			        <th>LASTERKETA/CARRERA</th>
			        <th>LUZERA/DISTANCIA</th>
			        <th>JAIOTZE URTEA/AÑO NACIMIENTO</th>
			      </tr>
			    </thead>
			    <tbody >
			      <tr class="active">
			        <td>12:00<br>12:00</td>
			        <td>JUVENILES M/F.<br/>SENIOR-VETER </td>
			        <td>5km- 1 vuelta</td>
			        <td>2000-2001<br/> 1999 y anteriores</td>
			      </tr>
			      <tr class="active">
			        <td>12:00</td>
			        <td>C.N.RUNNING <br/>SENIOR-VETER </td>
			        <td>10km- 2 vueltas</td>
			        <td>1999 y anteriores</td>
			      </tr>
			      
			      <tr class="warning">
			        <td>13:00</td>
			        <td>ENTREGA DE PREMIOS CARRERA ABSOLUTA </td>
			        <td></td>
			        <td></td>
			      </tr>
			      </tbody>
			      </table>
			  
			  
			  </div>
	</div>
		</div>
		</div>
		
		 
 		
		
		
		
		</div>
		


     <?php include_once 'componentes/footer.php';?>
  
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
  	<script type="text/javascript" src="js/gmaps.js"></script>
	<script type="text/javascript" src="js/smoothscroll.js"></script>
    <script type="text/javascript" src="js/jquery.parallax.js"></script>
    <script type="text/javascript" src="js/coundown-timer.js"></script>
    <script type="text/javascript" src="js/jquery.scrollTo.js"></script>
    <script type="text/javascript" src="js/jquery.nav.js"></script>
    <script type="text/javascript" src="js/main.js"></script>  
</body>
</html>