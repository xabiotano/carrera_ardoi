<?php 
$prefix="";
if(startsWith ($_SERVER['PHP_SELF'],"/inscripcion")){
	$prefix="/";
}


		
		
function startsWith($haystack, $needle) {
	// search backwards starting from haystack length characters from the end
	return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}

function endsWith($haystack, $needle) {
	// search forward starting from end minus needle length characters
	return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}
		
		
		
?>
<div class="collapse navbar-collapse">
	<ul class="nav navbar-nav navbar-right">
		<li class="scroll <?php echo ($_SERVER['PHP_SELF']=="/index.php") ? "active":"";?>"><a class="no-scroll" href="<?php echo $prefix;?>index.php">Inicio</a></li>
		<li class="scroll <?php echo (startsWith ($_SERVER['PHP_SELF'],"/inscripcion")) ? "active":"";?>"><a class="no-scroll" <?php echo "href= https://www.flickr.com/photos/11447224@N08/sets/72157667468248456";?> target="_blank">Fotos</a></li>
		<li class="scroll <?php echo ($_SERVER['PHP_SELF']=="/resultados.php") ? "active":"";?>"><a class="no-scroll" href="<?php echo $prefix;?>resultados.php">Resultados</a></li>
		<li class="scroll <?php echo ($_SERVER['PHP_SELF']=="/informacion.php") ? "active":"";?>"><a class="no-scroll" href="<?php echo $prefix;?>informacion.php">Información</a></li>
		<li class="scroll <?php echo ($_SERVER['PHP_SELF']=="/recorridos.php") ? "active":"";?>"><a class="no-scroll" href="<?php echo $prefix;?>recorridos.php">Recorridos</a></li>
		<li class="no-scroll <?php echo ($_SERVER['PHP_SELF']=="/reglamento.php") ? "active":"";?>"><a class="no-scroll" href="<?php echo $prefix;?>reglamento.php">Reglamento</a></li>
		<li class="scroll <?php echo ($_SERVER['PHP_SELF']=="/inscritos.php") ? "active":"";?>"><a class="no-scroll" class="no-scroll" href="<?php echo $prefix;?>inscritos.php">Inscritos</a></li>
	</ul>
	
</div>
