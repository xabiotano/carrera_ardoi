<?php 
	require_once 'inscripcion/classes/Carrera.php';
	require_once 'inscripcion/classes/ConfigCarrera.php';
	include_once ("../CCSantiago2017/conexion.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Resultados-CARRERA CAMINO DE SANTIAGO 2017 5/10KM</title>
     <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/noinscripcion.css" rel="stylesheet">
	<link href="css/animate.css" rel="stylesheet">	
	<link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <style>
		body{
			background-color:#19153d;
			color:#000;
		}
	</style> 
</head><!--/head-->

<body>
	<header id="header" role="banner">		
		<div class="main-nav">
			<div class="container">
				    
		        <div class="row">	        		
		            <div class="navbar-header">
		                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		                    <span class="sr-only">Toggle navigation</span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
		                <a class="navbar-brand" href="index.html">
		                	 <img  src="images/logo.png"/>
		                </a>                    
		            </div>
		             <?php 
		            	include_once 'componentes/navbar.php';
		            ?>
		        </div>
	        </div>
        </div>                    
    </header>
    <!--/#header--> 

    
 <br/>
      <br/>
      <br/>
      <br/>
      <br/>
      <br/>
      <br/>
	<div class="container">
      
	     <div class="row">
	      	<div class="col-md-12">
				
				<div class="panel panel-default">
				  <div class="panel-body">
                    	<?php
						$hayres="SELECT id FROM resultados limit 0,1";
						$hayre=mysql_query($hayres,$conexion);
						if(mysql_num_rows($hayre)>0){$res=1;}else{$res=0;}
                    	if($res==0){?>
                      <div class="alert alert-danger" role="alert">Clasificaciones aún no disponibles.</div>
						<?php }?>
				
				
				
	      	     <h3>Sacar hojas de resultados:</h3>
				<div class="btn-group">
				  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    Por carrera <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				    <li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=carrera&carrera=1';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Prebenjamín Masculina</a></li>
				    <li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=carrera&carrera=2';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Prebenjamín Femenina</a></li>
				    <li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=carrera&carrera=3';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Benjamín Masculina</a></li>
					<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=carrera&carrera=4';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Benjamín Femenina</a></li>
				    <li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=carrera&carrera=5';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Alevín Masculina</a></li>
					<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=carrera&carrera=6';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Alevín Femenina</a></li>
				    <li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=carrera&carrera=7';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Infantil</a></li>
					<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=carrera&carrera=8';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Cadetes y adaptados</a></li>
				    <li role="separator" class="divider"></li>
				    <li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=carrera&carrera=11';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Absoluta 5km</a></li>
					<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=carrera&carrera=10';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Absoluta 10km</a></li>
				  </ul>
				</div>
	      	     
				<div class="btn-group">
				  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    Por categoría (carreras infantiles) <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				    <li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=carrera&carrera=1&categoria=Prebenjamin%20M';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Prebenjamín M</a></li>
				    <li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=carrera&carrera=2&categoria=Prebenjamin%20F';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Prebenjamín F</a></li>
				    <li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=carrera&carrera=3&categoria=Benjamin%20M';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Benjamín M</a></li>
					<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=carrera&carrera=4&categoria=Benjamin%20F';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Benjamín F</a></li>
				    <li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=carrera&carrera=5&categoria=Alevin%20M';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Alevín M</a></li>
					<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=carrera&carrera=6&categoria=Alevin%20F';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Alevín F</a></li>
				    <li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=carrera&carrera=7&categoria=Infantil%20M';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Infantil M</a></li>
					<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=carrera&carrera=7&categoria=Infantil%20F';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Infantil F</a></li>
					<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=carrera&carrera=8&categoria=Cadete%20M';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Cadete M</a></li>
					<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=carrera&carrera=8&categoria=Cadete%20F';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Cadete F</a></li>
					<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=carrera&carrera=8&categoria=Adaptados%20M';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Adaptados M</a></li>
					<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=carrera&carrera=8&categoria=Adaptados%20F';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Adaptados F</a></li>
				  </ul>
				</div>
				
 		  		
 		
		
	    
	     <div class="row">
	      	<div class="col-md-12">
	      	     <h3>Carreras absolutas</h3>
				<div class="btn-group">
				  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    5Km <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				    <li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=5km&cri=Juvenil%20M';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Juvenil Masculina</a></li>
					<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=5km&cri=Juvenil%20F';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Juvenil Femenina</a></li>
				    <li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=5km&cri=Absoluta%20M';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Absoluta Masculina</a></li>
					<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=5km&cri=Absoluta%20F';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Absoluta Femenina</a></li>
				   
				</div>
				
				<div class="btn-group">
				  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    10Km <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				    <li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=10km&cri=Absoluta%20M';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Absoluta Masculina</a></li>
					<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=10km&cri=Absoluta%20F';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Absoluta Femenina</a></li>
				    <li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=10km&cri=Veteranos%20M';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Veterana Masculina</a></li>
					<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=10km&cri=Veteranas%20F';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Veterana Femenina</a></li>
				    <li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=10km&cri=Local%20M';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Local Masculina</a></li>
					<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=10km&cri=Local%20F';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Local Femenina</a></li>
				  </ul>
				</div>
				
			</div>
		</div>
	     
		
		
	     <div class="row">
	      	<div class="col-md-12">
				<div class="col-md-6">
	      	     <h3>Categorías Circuito Navarro de Running (Carrera de 10 Km)</h3>
				 </div>
				 <div class="col-md-2">
				 	<img class="img-responsive" src="images/logo-cnrunning.png" alt="CNRUnning" style="max-width:100px">
			 	</div>
			<div class="col-md-12">
				<div class="btn-group">
				  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    Hombres <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
			    	<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=circuito&mod=Ab&s=M';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Absoluta Masculino</a></li>
					<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=circuito&mod=JPS&s=M';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Junior Senior Promesa Masculino</a></li>
			   	   	<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=circuito&mod=VA&s=M';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Veteranos A (nacidos entre 1978 y 1982)</a></li>
					<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=circuito&mod=VB&s=M';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Veteranos B (nacidos entre 1973 y 1977)</a></li>
			   		<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=circuito&mod=VC&s=M';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Veteranos C (nacidos entre 1963 y 1972)</a></li>
					<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=circuito&mod=VD&s=M';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Veteranos D (nacidos antes de 1963)</a></li>
					
				  </ul>
				</div>
				<div class="btn-group">
				  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				   Mujeres <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
			    	<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=circuito&mod=Ab&s=F';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Absoluta Femenina</a></li>
					<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=circuito&mod=JPS&s=F';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Junior Senior Promesa Femenina</a></li>
			   	   	<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=circuito&mod=VA&s=F';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Veteranas A (nacidas entre 1978 y 1982)</a></li>
					<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=circuito&mod=VB&s=F';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Veteranas B (nacidas entre 1973 y 1977)</a></li>
			   		<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=circuito&mod=VC&s=F';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Veteranas C (nacidas entre 1963 y 1972)</a></li>
					<li><a href="<?php if($res>0){echo 'http://www.ardoi.es/CCSantiago2017/clasific.php?seleccion=circuito&mod=VD&s=F';}?>#" <?php if($res>0){echo "target='_blank'";}?>>Veteranas D (nacidas antes de 1963)</a></li>
				  </ul>
				</div><div class="btn-group" onClick="window.open('http://cnrunning.com/clasificacion-circuito/');">
				  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				   Clasificación General del Circuito <span class="caret"></span>
				  </button>
				  
				</div>
				
			</div>
		</div>
		
		  </div>
		</div>
	  <br/>
	  <br/>
	  <br/>
	  <br/>
		  </div>
		 
		</div>
		
		
		
	</div>
 		

		


     <?php include_once 'componentes/footer.php';?>
  
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
  	<script type="text/javascript" src="js/gmaps.js"></script>
	<script type="text/javascript" src="js/smoothscroll.js"></script>
    <script type="text/javascript" src="js/jquery.parallax.js"></script>
    <script type="text/javascript" src="js/coundown-timer.js"></script>
    <script type="text/javascript" src="js/jquery.scrollTo.js"></script>
    <script type="text/javascript" src="js/jquery.nav.js"></script>
    <script type="text/javascript" src="js/main.js"></script>  
</body>
</html>