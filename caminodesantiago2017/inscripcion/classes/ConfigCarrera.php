<?php

class ConfigCarrera{
	
	const ID_CARRERA=1;
	const CATEGORIA_5_KM=11;
	const CATEGORIA_10_KM=10;
	const CATEGORIA_INFANTIL=0;
	const URL_CARRERA='http://carrera.ardoi.es/';
	const URL_INSCRIPCION_OK='http://carrera.ardoi.es/inscripcion/InscripcionOk.php';
	const URL_INFORMACION_CARRERA='http://carrera.ardoi.es/informacion.php';
	const URL_ELEGIR_CARRERA='http://carrera.ardoi.es/inscripcion/elegirCarrera.php';
	const NIF_INFANTIL='11111111H';
	const ANO_JUVENIL1=2000;
	const ANO_JUVENIL2=2001;
	const DIA_CARRERA='02/04/2017';
	const URL_WS_IMPRESION='http://www.ardoi.es/CCSantiago2017/justificanteInscripcion.php?atleta=';
	const URL_INSCRITOS='http://carrera.ardoi.es/inscritos.php';
}

?>