<?php

	if(isset($pagoInscripcion)){
		die("Hace falta un objeto de inscripcion");
	}
	// Se incluye la librería
	include '../apiredsys/apiRedsys.php';
	// Se crea Objeto
	$miObj = new RedsysAPI;
		
	// Valores de entrada
	$fuc="159167998"; //cogido de la consola
	$terminal="001"; // cogido del correo
	$moneda="978"; //978€
	$trans="0"; //0 – Autorización
	$url="http://carrera.ardoi.es/callback/recibirNotificacion.php";	//Obligatorio si el comercio tiene notificación “on-line”. URL del comercio que recibirá un post con los datos de la transacción.
	$urlOK="http://carrera.ardoi.es/InscripcionOk.php";
	$urlKO="http://carrera.ardoi.es/InscripcionKo.php";
	
	//Opcional: si se envía será utilizado como URLOK ignorando el configurado en el módulo de administración en caso de tenerlo.
				//Opcional: si se envía será utilizado como URLKO ignorando el configurado en el módulo de administración en caso de tenerlo
	$id=time();// ID de la orden , debemos generarla en la BD para meter la inscripccion como NO PAGADA antes
	$amount="1250";
	$productoDescripcion="Inscripcion Carrera Camino de Santiago-Ardoi";//
	$productoTitular="";
	
	// Se Rellenan los campos
	$miObj->setParameter("DS_MERCHANT_AMOUNT",$amount);
	$miObj->setParameter("DS_MERCHANT_ORDER",strval($id));
	$miObj->setParameter("DS_MERCHANT_MERCHANTCODE",$fuc);
	$miObj->setParameter("DS_MERCHANT_CURRENCY",$moneda);
	$miObj->setParameter("DS_MERCHANT_TRANSACTIONTYPE",$trans);
	$miObj->setParameter("DS_MERCHANT_TERMINAL",$terminal);
	$miObj->setParameter("DS_MERCHANT_MERCHANTURL",$url);
	$miObj->setParameter("DS_MERCHANT_URLOK",$urlOK);		
	$miObj->setParameter("DS_MERCHANT_URLKO",$urlKO);
	$miObj->setParameter("Ds_Merchant_ProductDescription",$productoDescripcion);
	$miObj->setParameter("Ds_Merchant_Titular",$productoTitular);
	

	//Datos de configuración
	$version="HMAC_SHA256_V1";
	$kc = 'Mk9m98IfEblmPfrpsawt7BmxObt98Jev';//Clave recuperada de CANALES
	// Se generan los parámetros de la petición
	$params = $miObj->createMerchantParameters();
	$signature = $miObj->createMerchantSignature($kc);

 
?>
<html lang="es">
<head>
</head>
<body>
<form name="frm" action="http://sis-d.redsys.es/sis/realizarPago" method="POST" target="_blank">
Ds_Merchant_SignatureVersion <input type="text" name="Ds_SignatureVersion" value="<?php echo $version; ?>"/></br>
Ds_Merchant_MerchantParameters <input type="text" name="Ds_MerchantParameters" value="<?php echo $params; ?>"/></br>
Ds_Merchant_Signature <input type="text" name="Ds_Signature" value="<?php echo $signature; ?>"/></br>
<input type="submit" value="Enviar" >
</form>

</body>
</html>
