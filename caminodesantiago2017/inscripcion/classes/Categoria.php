<?php
	require_once 'Database.php';	
	require_once 'ConfigCarrera.php';
			
class Categoria{
	
	const NOMBRE_TABLA_CATEGORIAS="carrera";
	const CATEGORIA_5_KM=ConfigCarrera::CATEGORIA_5_KM;
	const CATEGORIA_10_KM=ConfigCarrera::CATEGORIA_10_KM;
	const CATEGORIA_INFANTIL=ConfigCarrera::CATEGORIA_INFANTIL;
	
	
	
	var $absolut;
	var $años;
	var $carrera;
	var $carreraeu;
	var $cat;
	var $cuota;
	var $distancia;
	var $horario;
	var $hora_resultados;
	var $id;
	var $rango_dorsales;
	var $time_inicio;
	var $urteak;
	
	public function __construct($id_categoria)
	{
		try
		{
			$db=new Database();
			$categoria=$db->custom_query("SELECT * FROM ".Categoria::NOMBRE_TABLA_CATEGORIAS." WHERE id=".$id_categoria);
			$this->absolut=$categoria->absolut;
			$this->años=$categoria->años;
			$this->carrera=$categoria->carrera;
			$this->carreraeu=$categoria->carreraeu;
			$this->cat=$categoria->cat;
			$this->cuota=$categoria->cuota;
			$this->distancia=$categoria->distancia;
			$this->horario=$categoria->horario;
			$this->hora_resultados=$categoria->hora_resultados;
			$this->id=$categoria->id;
			$this->rango_dorsales=$categoria->rango_dorsales;
			$this->time_inicio=$categoria->time_inicio;
			$this->urteak=$categoria->urteak;
		}catch (Exception $e){
			echo "Error creando categoria";
		}
	}
	
	
	function GetMinDorsalAsignable(){
		
		if($this->rango_dorsales=="0.0000"){
			$carrera=new Carrera(ConfigCarrera::ID_CARRERA);
			$this->rango_dorsales=$carrera->rango_dorsales_infantiles;
			echo "aaa".$carrera->rango_dorsales_infantiles.$this->rango_dorsales;
		}
		$rangos = explode("-", $this->rango_dorsales);
		if(sizeof($rangos)!=2){
			echo "Error obteniendo rangos";
			die();
		}
		//es el minimo +1 ya que
		//el dorsal 1 es para el ganador
		//el dorsal 500 no existe!!
		return (intval($rangos[0])+1);
	}
	
	function GetMaxDorsalAsignable(){
		if($this->rango_dorsales=="0.0000"){
			$carrera=new Carrera(ConfigCarrera::ID_CARRERA);
			$this->rango_dorsales=$carrera->rango_dorsales_infantiles;
		}
		$rangos = explode("-", $this->rango_dorsales);
		if(sizeof($rangos)!=2){
			echo "Error obteniendo rangos";
			die();
		}
		return intval($rangos[1]);
	}
	
	
}