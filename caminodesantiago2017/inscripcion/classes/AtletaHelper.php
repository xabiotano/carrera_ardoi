<?php

	require_once 'PagoInscripcion.php';
/*

while($as=mysql_fetch_array($dat)){
	$titulo_carrera=$as["titulo_carrera"];
	$titulo_carrera_euskera=$as["titulo_carrera_euskera"];
	$fecha=$as["fecha"];
	$diafinalinscripcion=substr($fecha,0,4)."-".substr($fecha,5,2)."-".(substr($fecha,-2,2)-2);
}
*/



//EJEMPLO :$categorias=AtletaHelper::GetCategorias('04/07/1985', "M", '24/06/2016',0);
/////////////////////////////////////////////////   categorias  //////////////////////////////////////////////////////////////////
class AtletaHelper{
	
	//se espera de parametro $fecha_nacimiento=formato "yyyy-mm-dd"
	//$sexo M m /f F
	static function GetCategorias($fecha_nacimiento,$sexo,$fecha_carrera,$adaptados){
		
		//limpieza y extraccion de campos
		$date = DateTime::createFromFormat("d/m/Y", $fecha_nacimiento);
		
		$anonac= $date->format("Y");
		$mesnac= $date->format("m");
		$dianac= $date->format("d");
		if($sexo=="M" OR $sexo=="m"){$sexo="M";}
		if($sexo=="F" OR $sexo=="f"){$sexo="F";}
		
		$fecha_carrera = DateTime::createFromFormat("d/m/Y", $fecha_carrera);
		
		$diacarrera= $fecha_carrera->format("d");
		$anocarrera=$fecha_carrera->format("Y");
		
		$cumple=$mesnac."-".$dianac;
		//definimos edadcarrera
		if($cumple>$diacarrera){$edadcarrera=$anocarrera-$anonac-'1';}else{$edadcarrera=$anocarrera-$anonac;}
		if($edadcarrera>34){if($sexo=="M"){$categoria="Veterano M";}else{$categoria="Veterana F";}}
		
		if($anonac>2009){if($sexo=="M"){$categoria="Prebenjamin M";}else{$categoria="Prebenjamin F";}}
		if($anonac=="2008" OR $anonac=="2009" ){if($sexo=="M"){$categoria="Benjamin M";}else{$categoria="Benjamin F";}}
		if($anonac=="2006" OR $anonac=="2008" ){if($sexo=="M"){$categoria="Alevin M";}else{$categoria="Alevin F";}}
		if($anonac=="2004" OR $anonac=="2005" ){if($sexo=="M"){$categoria="Infantil M";}else{$categoria="Infantil F";}}
		if($anonac=="2002" OR $anonac=="2003" ){if($sexo=="M"){$categoria="Cadete M";}else{$categoria="Cadete F";}}
		if($anonac=="2000" OR $anonac=="2001" ){if($sexo=="M"){$categoria="Juvenil M";}else{$categoria="Juvenil F";}}
		if($anonac=="1998" OR $anonac=="1999" ){if($sexo=="M"){$categoria="Junior M";}else{$categoria="Junior F";}}
		if($anonac=="1995" OR $anonac=="1996" OR $anonac=="1997"){if($sexo=="M"){$categoria="Promesa M";}else{$categoria="Promesa F";}}
		if($anonac<'1995' AND $edadcarrera<'35' ){if($sexo=="M"){$categoria="Senior M";}else{$categoria="Senior F";}}
		if($adaptados=="1"){$categoria="Adaptados"; if($sexo=="M"){$categoria="Adaptados M";}else{$categoria="Adaptados F";}}
		
		////////////////////   CAT ////////////////
		$cat=$categoria;
		if(substr($categoria,0,3)=="Inf"){$cat="Infantil";}
		if(substr($categoria,0,3)=="Cad"){$cat="Cadetes y Adaptados";}
		if($sexo=="F" AND $anonac<2002){$cat="Absoluta F";}
		if($sexo=="M" AND $anonac<2002){$cat="Absoluta M";}
		if($adaptados=="1"){$cat="Cadetes y Adaptados";}
		
		
		$devolver["categoria"]=$categoria;
		$devolver["cat"]=$cat;
		return $devolver;
	}
	
	
	


}


/*

$diacarrera=substr($fecha,5,5);
$anocarrera=substr($fecha,0,4);
$cumple=$mesnac."-".$dianac;

if($cumple>$diacarrera){$edadcarrera=$anocarrera-$anonac-'1';}else{$edadcarrera=$anocarrera-$anonac;}
if($edadcarrera>34){if($sexo=="M"){$categoria="Veterano M";}else{$categoria="Veterana F";}}


if($anonac>2008){if($sexo=="M"){$categoria="Prebenjamin M";}else{$categoria="Prebenjamin F";}}
if($anonac=="2007" OR $anonac=="2008" ){if($sexo=="M"){$categoria="Benjamin M";}else{$categoria="Benjamin F";}}
if($anonac=="2005" OR $anonac=="2006" ){if($sexo=="M"){$categoria="Alevin M";}else{$categoria="Alevin F";}}
if($anonac=="2003" OR $anonac=="2004" ){if($sexo=="M"){$categoria="Infantil M";}else{$categoria="Infantil F";}}
if($anonac=="2001" OR $anonac=="2002" ){if($sexo=="M"){$categoria="Cadete M";}else{$categoria="Cadete F";}}
if($anonac=="1999" OR $anonac=="2000" ){if($sexo=="M"){$categoria="Juvenil M";}else{$categoria="Juvenil F";}}
if($anonac=="1997" OR $anonac=="1998" ){if($sexo=="M"){$categoria="Junior M";}else{$categoria="Junior F";}}
if($anonac=="1995" OR $anonac=="1996" OR $anonac=="1994"){if($sexo=="M"){$categoria="Promesa M";}else{$categoria="Promesa F";}}
if($anonac<'1995' AND $edadcarrera<'35' ){if($sexo=="M"){$categoria="Senior M";}else{$categoria="Senior F";}}
if($adaptados=="1"){$categoria="Adaptados"; if($sexo=="M"){$categoria="Adaptados M";}else{$categoria="Adaptados F";}}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////   errores    ///////////////////////////////////////////////////////////////////


if($nombre==''){$error=1; $textoerror="Has olvidado poner el nombre";}
if($apellido1==''){$error=1; $textoerror="Has olvidado poner el apellido";}
if($nombre==''){$error=1; $textoerror="Has olvidado poner el nombre";}
if($mesnac=='04' AND $dianac>'30'){$error=1; $textoerror="La fecha indicada no existe. Revísala de nuevo";}
if($mesnac=='06' AND $dianac>30){$error=1; $textoerror="La fecha indicada no existe. Revísala de nuevo";}
if($mesnac=='09' AND $dianac>30){$error=1; $textoerror="La fecha indicada no existe. Revísala de nuevo";}
if($mesnac=='11' AND $dianac>30){$error=1; $textoerror="La fecha indicada no existe. Revísala de nuevo";}
if(date("Y-m-d")>$diafinalinscripcion AND $oficina!="1"){$error=1; $textoerror="El plazo de inscripción por internet ha terminado.";}

$bisi=$anonac/4;

if(strlen($bisi)>3){if($mesnac=='02' AND $dianac>28){$error=1; $textoerror="La fecha indicada no existe. Revísala de nuevo";}}
if(strlen($bisi)==3){if($mesnac=='02' AND $dianac>29){$error=1; $textoerror="La fecha indicada no existe. Revísala de nuevo";}}
if($sexo=='-'){$error=1; $textoerror="Has olvidado decirnos si eres chico o chica";}
if($error==1){echo $textoerror; exit;}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////           datos               ////////////////////////////////////////////////////////


$nombre  = strtolower($nombre);      $nombre  = ucwords($nombre);
$apellido1=strtolower($apellido1);   $apellido1=ucwords($apellido1);
$apellido2=strtolower($apellido2);   $apellido2=ucwords($apellido2);
//$club   =  strtolower($club);        $club   =  ucwords($club);
$localidad=strtolower($localidad);   $localidad=ucwords($localidad);

if($local=="Sí") {$local="local";} else {;}

$fecha_nac=$anonac."-".$mesnac."-".$dianac;

if($via=="internet"){if ($h=="eu"){$fe_ins=date("M-d H:i")."-eusk";}else{$fe_ins=date("M-d H:i")."-cast";}}
if($via=="oficina"){$fe_ins=date("M-d H:i")."-ofi";}

$cat=$categoria;
if(substr($categoria,0,3)=="Inf"){$cat="Infantil";}
if(substr($categoria,0,3)=="Cad"){$cat="Cadetes y Adaptados";}
if($sexo=="F" AND $anonac<2001){$cat="Absoluta F";}
if($sexo=="M" AND $anonac<2001){$cat="Absoluta M";}
if($adaptados=="1"){$cat="Cadetes y Adaptados";}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if($corr=="1")
{$sql="UPDATE atleta SET nombre='$nombre',apellido1='$apellido1',apellido2='$apellido2',fecha_nac='$fecha_nac',categoria='$categoria',cat='$cat',club='$club',localidad='$localidad',local='$local',via='$via',telefono='$telefono',pago='$pago',dorsal='$dorsal' WHERE id=$atleta";}

else{
	$sql="INSERT INTO atleta(nombre,apellido1,telefono,apellido2,fecha_nac,categoria,cat,club,localidad,local,via,pago,dorsal,fe_ins)";
	$sql.="VALUES ('$nombre','$apellido1','$telefono','$apellido2','$fecha_nac','$categoria','$cat','$club','$localidad','$local','$via','$pago','$dorsal','$fe_ins')";}

	mysql_query($sql,$conexion);

	if (mysql_errno($conexion)==0){$error="0";}else{$error="1";}
	if($error=="0" AND $corr=="1"){echo "La modificación ya está hecha. Pulsa 'Aceptar'";}

	$atl=mysql_query("SELECT * FROM atleta order by id desc limit 0,1",$conexion);
	while($tal=mysql_fetch_array($atl)){$ida=$tal["id"];}
	?>

<script language="JavaScript">
  function fichatleta() {
    window.open('atleta.php?atleta=<?php echo $ida;?>&ofi=1', 'popup<?php echo $ida;?>', 'width=450, height=350, top=40, left=500, menubar=0, scrollbars=0, location=0, toolbar=0,  resizable=0, status=0');
  }
</script>
<?php

if($error=="0")
	{if ($via=="oficina" AND $corr!="1") {?>
 <p class='Estilo1' align='center'><strong>La inscripción de <a href="#" onClick="fichatleta(''); return false;"><?php echo $nombre." ".$apellido1;?></a> se ha realizado correctamente</strong></p><?php ;//include("inscripciones_oficina.php");
} else {/*include("inscrito.php")*/; /*}}*/
?>