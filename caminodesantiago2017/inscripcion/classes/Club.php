<?php

require_once 'Database.php';

class Club{
	
	const NOMBRE_TABLA_CLUBS="clubes";
	
	static function GetClubs(){
	
		$db=new Database();
		$clubs=$db->fetch_all(Club::NOMBRE_TABLA_CLUBS );
		return $clubs;
	}
	
	
	
}
