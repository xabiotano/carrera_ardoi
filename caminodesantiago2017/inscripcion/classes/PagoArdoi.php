<?php
class PagoArdoi {
    
	var $importe;
    

   
	
	function getImporte() {
		if(is_empty($importe)){
			throw new Exception('No se ha definido el Importe');
		}
		return $importe;
	}
	
	function PagoArdoi() {
        $this->importe =0;
    }
    
    
    
    static function getInfoDsResponse($Ds_Code){
    	$Ds_CodeNumber=intval($Ds_Code);
    	$resultadoText="Transacción autorizada para pagos y preautorizaciones";
    	$resultadoExito=false;
    	if( $Ds_CodeNumber>=0 && $Ds_CodeNumber<=99){
    		$resultadoExito=true;
    		$resultadoText="Transacción autorizada para pagos y preautorizaciones";
    	}
    	if($Ds_CodeNumber==900){
    		$resultadoText="Transacción autorizada para devoluciones y confirmaciones";
    		$resultadoExito=true;
    	}else if($Ds_CodeNumber==400){
    		$resultadoText="Transacción autorizada para anulaciones";
    		$resultadoExito=true;
    	}else if($Ds_CodeNumber==101){
    		$resultadoText="Tarjeta caducada";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==102){
    		$resultadoText="Tarjeta en excepción transitoria o bajo sospecha de fraude";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==106){
    		$resultadoText="Intentos de PIN excedidos";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==125){
    		$resultadoText="Tarjeta no efectiva";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==129){
    		$resultadoText="Código de seguridad (CVV2/CVC2) incorrecto";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==180){
    		$resultadoText="Tarjeta ajena al servicio";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==184){
    		$resultadoText="Error en la autenticación del titular";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==190){
    		$resultadoText="Denegación del emisor sin especificar motivo";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==191){
    		$resultadoText="Fecha de caducidad errónea";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==202){
    		$resultadoText="Tarjeta en excepción transitoria o bajo sospecha de fraude con retirada de tarjeta";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==904){
    		$resultadoText="Comercio no registrado en FUC";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==909){
    		$resultadoText="Error de sistema";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==913){
    		$resultadoText="Pedido repetido";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==944){
    		$resultadoText="Sesión Incorrecta";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==950){
    		$resultadoText="Operación de devolución no permitida";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==9912 || $Ds_CodeNumber==912){
    		$resultadoText="Emisor no disponible";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==9064){
    		$resultadoText="Número de posiciones de la tarjeta incorrecto";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==9078){
    		$resultadoText="Tipo de operación no permitida para esa tarjeta";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==9093){
    		$resultadoText="Tarjeta no existente";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==9094){
    		$resultadoText="Rechazo servidores internacionales";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==9104){
    		$resultadoText="Comercio con 'titular seguro' y titular sin clave de compra segura";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==9218){
    		$resultadoText="El comercio no permite op. seguras por entrada /operaciones";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==9253){
    		$resultadoText="Tarjeta no cumple el check-digit";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==9256){
    		$resultadoText="El comercio no puede realizar preautorizaciones";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==9257){
    		$resultadoText="Esta tarjeta no permite operativa de preautorizaciones";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==9261){
    		$resultadoText="Operación detenida por superar el control de restricciones en la entrada al SIS";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==9913){
    		$resultadoText="Error en la confirmación que el comercio envía al TPV Virtual (solo aplicable en la opción de sincronización SOAP)";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==9915){
    		$resultadoText="A petición del usuario se ha cancelado el pago";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==9928){
    		$resultadoText="Anulación de autorización en diferido realizada por el SIS (proceso batch)";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==9929){
    		$resultadoText="Anulación de autorización en diferido realizada por el comercio";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==9997){
    		$resultadoText="Se está procesando otra transacción en SIS con la misma tarjeta";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==9998){
    		$resultadoText="Operación en proceso de solicitud de datos de tarjeta";
    		$resultadoExito=false;
    	}else if($Ds_CodeNumber==9999){
    		$resultadoText="Operación que ha sido redirigida al emisor a autenticar";
    		$resultadoExito=false;
    	}
    	$devolver=array();
    	$devolver["resultadoText"]=$resultadoText;
    	$devolver["resultadoExito"]=$resultadoExito;
    	return $devolver;
    }
    
    
    
}
?>
