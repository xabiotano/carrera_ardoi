<?php
	require_once 'Database.php';

class Carrera{
	
	const NOMBRE_TABLA_DATOS_CARRERA="datos";
	
	var $id;
	var $titulo_carrera;
	var $titulo_carrera_euskera;
	var $fecha;
	var $ultimodiainscripcion;
	var $ruta_titulo;
	var $rango_dorsales_infantiles;
	var $primerAnoAbsoluto;
	
	
	public function __construct($id)
	{
		try
		{
			$db=new Database();
			$datosCarrera=$db->custom_query("SELECT * FROM ".Carrera::NOMBRE_TABLA_DATOS_CARRERA." WHERE id=".$id);
			
			$this->id=$datosCarrera->id;
			$this->titulo_carrera=$datosCarrera->titulo_carrera;
			$this->titulo_carrera_euskera=$datosCarrera->fecha;
			$this->fecha=DateTime::createFromFormat('Y-m-d', $datosCarrera->fecha);
			$this->ultimodiainscripcion=DateTime::createFromFormat('Y-m-d', $datosCarrera->ultimodiainscripcion);
			$this->rango_dorsales_infantiles=$datosCarrera->rango_dorsales_infantiles;
			$this->primerAnoAbsoluto=$datosCarrera->primerAnoAbsoluto;
			
		} catch (PDOException $e)
		{
			echo "error creando Carrera " . $e->getMessage();
		}
	}
	
	
	function SePuedeInscribir(){
		$hoy=date;
		if($hoy>$this->ultimodiainscripcion){
			return false;
		}else{
			return true;
		}
		
	}
	
	
	function GetMinDorsalAsignable(){
		echo "OK".$this->rango_dorsales_infantiles;
		$rangos = explode("-", $this->rango_dorsales_infantiles);
		if(sizeof($rangos)!=2){
			echo "Error obteniendo rangos(Carrera)";
			die();
		}
		return intval($rangos[0]);
	}
	
	
	
	

	
}


