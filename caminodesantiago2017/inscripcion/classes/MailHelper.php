<?php

require 'PHPMailer/PHPMailerAutoload.php';
require_once 'ConfigCarrera.php';

 class MailHelper{
 	
 	static function SendMail($to,$IdInscripcion){
 		$devolver=true;
 		try{
 			
 			$mail = new PHPMailer(); // defaults to using php "mail()"
 			
 			$body= file_get_contents('emailcontent/index.html');
 			//$body = eregi_replace("[\]",'',$body);
 			//$body="<html><p>".time()."</p></html>";
 			//$mail->AddReplyTo("ardoi@yourdomain.com","First Last");
 			$mail->MsgHTML($body);
 			
 			$mail->SetFrom('no-reply@ardoi.es', 'Justificante Inscripcion - XXIV Carrera Camino de Santiago 2017');
 			
 			//$mail->AddReplyTo("name@yourdomain.com","First Last");
 			
 			$address = $to;
 			$mail->AddAddress($address, $to);
 			
 			$mail->Subject    = "Justificante Inscripcion - XXIV Carrera Camino de Santiago 2017";
 			
 			$mail->AltBody    = "Descargue el justificante de su inscripcion."; // optional, comment out and test
 			
 			$IdInscripcion=intval($IdInscripcion);
 			$pdf=curl_get_contents(ConfigCarrera::URL_WS_IMPRESION.$IdInscripcion);
 			
 			$nombre='Justificante_'.$IdInscripcion.'.pdf';
 			$mail->addStringAttachment($pdf,$nombre);
 			
 		
 		
 			
 			$mail->AddEmbeddedImage("emailcontent/img/cartelcarrera2017.jpg",'cartelcarrera2017');
 			$mail->AddEmbeddedImage("emailcontent/img/facebook.png","facebook");
 			$mail->AddEmbeddedImage("emailcontent/img/twitter.png","twitter");
 			$mail->AddEmbeddedImage("emailcontent/img/logo.jpg","logo");
 			
 			
 			
 			if(!$mail->Send()) {
 				//echo "Mailer Error: " . $mail->ErrorInfo;
 			} else {
 				//echo "Message sent!";
 			}
 			
 			
 		}catch(Exception $e){
 			$devolver=true;
 			echo $e;
 		}
 	}
}


function curl_get_contents($url)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_URL, $url);
	$data = curl_exec($ch);
	curl_close($ch);
	return $data;
}


?>