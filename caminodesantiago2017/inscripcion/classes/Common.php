<?php
	require_once 'ConfigCarrera.php';
	 function RedirigirAError($error){
	 	header("Location:Error.php?error=".urlencode($error));
	 	die();
	 }
	 
	 function RedirigirAInicio(){
	 	header("Location: ".ConfigCarrera::URL_ELEGIR_CARRERA);
	 	die();
	 }
	 
	 