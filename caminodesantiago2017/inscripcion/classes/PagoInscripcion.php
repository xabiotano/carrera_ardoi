<?php

	require_once 'Database.php';
	require_once 'PagoArdoi.php';
	require_once 'AtletaHelper.php';
	require_once 'Categoria.php';
	require_once 'Carrera.php';
	
class PagoInscripcion extends PagoArdoi
{
	const NOMBRE_TABLA_INSCRITOS="atleta";
	
	const ID_CARRERA=1;
	

	
	var $Dorsal;
	var $Tarifa;
	var $id;
	var $Nombre;
	var $Apellido1;
	var $Apellido2;
	var $Genero;
	var $FechaNacimiento;
	var $Documento;
	var $Email;
	var $Movil;
	var $Poblacion;
	var $Provincia;
	var $Club;
	var $ViveEnZizur;
	var $TallaCamiseta;
	var $Carrera_eleg; //vale los valores de ConfigCarrera{ 10=>10km ,11=>5km,0=>infantiles}
	var $ImporteTPV;	
	
	function PagoInscripcion(	 	$Tarifa,
									$Nombre,
									$Apellido1,
									$Apellido2,
									$Genero,
									$FechaNacimiento,
									$Documento,
									$Email,
									$Movil,
									$Poblacion,
									$Provincia,
									$Club,
									$ViveEnZizur,
									$TallaCamiseta)
    {
    	$this->FechaNacimiento= $FechaNacimiento;
    	$this->Documento=$Documento ;
    	//propios de esta clase
		$this->Dorsal=$Dorsal;
		$this->Tarifa= $Tarifa;
		$this->NumeroInscripcion=$NumeroInscripcion ;
		$this->Nombre= $this->limpiarCampoYMayus($Nombre);
		$this->Apellido1= $this->limpiarCampoYMayus($Apellido1);
		$this->Apellido2= $this->limpiarCampoYMayus($Apellido2) ;
		$this->Genero= $Genero;
		
		
		$this->Email=$Email ;
		$this->Movil= $Movil;
		$this->Poblacion= $this->limpiarCampoYMayus($Poblacion);
		$this->Provincia= $this->limpiarCampoYMayus($Provincia );
		$this->Club= $Club;
		$this->ViveEnZizur= $ViveEnZizur;
		$this->TallaCamiseta=$TallaCamiseta;
		$this->AsignarCarreraElegidaEImporte($Tarifa);
    }
  
    /**
	 * AsignarCarreraElegidaEImporte
	 *
	 * Asigna Carrera_eleg y importe al objeto PagoInscripcion
	 *
	 * @param (mixed) (TipoInscripcion) el tipo de inscripcion elegida 5 10 'I'
	 * @return (void)
	 */
	function AsignarCarreraElegidaEImporte($TipoInscripcion){
    	if($TipoInscripcion==5){
    		$fechaNac=DateTime::createFromFormat("Y-m-d", $this->FechaNacimiento);
    		if( $fechaNac->format('Y')==ConfigCarrera::ANO_JUVENIL1 ||
    				$fechaNac->format('Y')==ConfigCarrera::ANO_JUVENIL2
    				){
    					//es un juvenil por lo que paga 0
    					$this->importe=0;
    					$this->Documento=ConfigCarrera::NIF_INFANTIL;//no hace falta pero le asigno un NIF por defecto
    		}else{
    			$categoria=new Categoria(Categoria::CATEGORIA_5_KM);
    			$this->importe =$categoria->cuota;
    		}
    		$this->Carrera_eleg=Categoria::CATEGORIA_5_KM;
    	}else if ($TipoInscripcion==10){
    		$categoria=new Categoria(Categoria::CATEGORIA_10_KM);
    		$this->importe =$categoria->cuota;
    		$this->Carrera_eleg=Categoria::CATEGORIA_10_KM;
    	}else if($TipoInscripcion=="I"){
    		//es infantil
    		$fechaNac=DateTime::createFromFormat("Y-m-d", $this->FechaNacimiento);
    		
    		if( $fechaNac->format('Y')==ConfigCarrera::ANO_JUVENIL1 ||
    				$fechaNac->format('Y')==ConfigCarrera::ANO_JUVENIL2
    				){
    					//es un juvenil
    					$this->Carrera_eleg=Categoria::CATEGORIA_5_KM;
    					$this->Documento=ConfigCarrera::NIF_INFANTIL;//no hace falta pero le asigno un NIF por defecto
    		}else{
    			
    			$this->Carrera_eleg=Categoria::CATEGORIA_INFANTIL;
    		}
    		$this->importe =0;
    	}else{
    		echo "Categoria no reconocida";
    		die();
    	}
    	//para que el TPV funcione bien
    	$this->ImporteTPV=$this->importe*100;
    }
    
    function limpiarCampoYMayus($campo){
    	$campo=strtolower($campo);
    	$campo=ucwords($campo);
    	return $campo;
    }
    
    function insertarInscripcion(){
    	$devolver=-1;
    	try{
    		$arrayInscripcion= array();
			$arrayInscripcion["dni"]=$this->Documento;
    		$arrayInscripcion["nombre"]=($this->Nombre);
    		$arrayInscripcion["apellido1"]=($this->Apellido1);
    		$arrayInscripcion["apellido2"]=($this->Apellido2);
    		$arrayInscripcion["fecha_nac"]=$this->FechaNacimiento;
    		$arrayInscripcion["telefono"]=$this->Movil;
    		$arrayInscripcion["email"]=$this->Email;
    		$arrayInscripcion["club"]=$this->Club;
    		$arrayInscripcion["local"]=$this->ViveEnZizur;
    		$arrayInscripcion["localidad"]=($this->Poblacion);
    		$arrayInscripcion["via"]="internet";
    		$arrayInscripcion["pago"]="";//vacio por defecto
    		$arrayInscripcion["dorsal"]="";//sin asignar
       		$arrayInscripcion["fe_ins"]=date("m-d H:i")."-cast";//por ahora esta solo en castellano
    		//asigno las categorias
    		$fechaNac=DateTime::createFromFormat("Y-m-d", $this->FechaNacimiento);
			$fechaNac=$fechaNac->format('d/m/Y');
    		$diaCarrera=ConfigCarrera::DIA_CARRERA;
			$categorias=AtletaHelper::GetCategorias($fechaNac,$this->Genero, $diaCarrera,0);
    		$arrayInscripcion["categoria"]=$categorias["categoria"];
    		$arrayInscripcion["categoria"]=PagoInscripcion::limpiarTildes($arrayInscripcion["categoria"]);
    		$arrayInscripcion["cat"]=$categorias["cat"];
			//fin asignar las categorias
    		$arrayInscripcion["carrera_eleg"]=$this->Carrera_eleg;
    		$arrayInscripcion["dni"]= $this->Carrera_eleg!=0 ? $this->Documento:"";
    		
    		$db=new Database();
    		$this->NumeroInscripcion=$db->insert(PagoInscripcion::NOMBRE_TABLA_INSCRITOS,$arrayInscripcion);
    		if(!is_numeric($this->NumeroInscripcion)){
    			throw new Exception("No se ha podido guardar la inscripcion($this->NumeroInscripcion)");
    		}
    		//añado 0s delante hasta que tenga por lo menos 6 caracteres, hace falta para que funcione el TPV
    		while(strlen((string)$this->NumeroInscripcion)<=6){
    			$this->NumeroInscripcion="0".$this->NumeroInscripcion;
    		}
    		$devolver= $this->NumeroInscripcion;
    	}catch(Exception $e){
    		$this->NumeroInscripcion=-1;
    	}
    	return $devolver;
    }
    
    
    static function limpiarTildes($string){
    	$string=str_replace('í', 'i', $string);
    	$string=str_replace('é', 'e', $string);
    	$string=str_replace('á', 'a', $string);
    	$string=str_replace('ú', 'u', $string);
    	$string=str_replace('ó', 'o', $string);
    	return $string;
    }
   static function EstablecerPagado($NumeroInscripcion,$Ds_Order){
    	$db=new Database();
    	$db->update(self::NOMBRE_TABLA_INSCRITOS,array("Ds_order"=>$Ds_Order, "pago"=>"pagado"), "id", $NumeroInscripcion);
    }
    
    static function EstablecerPagoFallido($NumeroInscripcion,$Ds_Order){
    	$db=new Database();
    	$db->update(self::NOMBRE_TABLA_INSCRITOS,array("Ds_order"=>$Ds_Order, "pago"=>"fallido"), "id", $NumeroInscripcion);
    }
        
    
    static function GetInscripcionByNumero($NumeroInscripcion){
    	$db=new Database();
    	$devolver=$db->fetch_single_row(PagoInscripcion::NOMBRE_TABLA_INSCRITOS, "id",$NumeroInscripcion);
    	return $devolver;
    	
    }
    
    
    static function GetInscritos($filtro){
    	$filtroArra=array();
    	$filtroArra["pago"]="pagado";
    	$sql="SELECT * FROM ".PagoInscripcion::NOMBRE_TABLA_INSCRITOS. " WHERE pago='pagado'";
    	if(isset($filtro) && is_numeric($filtro)){
    		//filtro solo puede vale 10 11 o 0
    		$sql .=" AND carrera_eleg=".intval($filtro);
    		
    	}
    	$sql .=" ORDER BY dorsal";
    	$db=new Database();
    	$devolver=$db->custom_querySinFetch($sql);
    	return $devolver;
    	 
    }
    
    static function SetDorsal($NumeroInscripcion){
    	$db=new Database();
    	$NumeroInscripcion=intval($NumeroInscripcion);
    	
    	//obtengo la carrera_elegida y su categoria
    	$carrera_eleg_corredor=$db->custom_query("SELECT carrera_eleg  FROM ".PagoInscripcion::NOMBRE_TABLA_INSCRITOS." WHERE id='$NumeroInscripcion'");
    	if(intval($carrera_eleg_corredor->carrera_eleg)==ConfigCarrera::CATEGORIA_INFANTIL){
    		//si es infantil el rango dorsal lo cogemos de Carrera 
    		$carrera=new Carrera(ConfigCarrera::ID_CARRERA);
    		$minimoDorsalAsignable=$carrera->GetMinDorsalAsignable();
    	}else{
    		//sino de la categoria
    		$categoria_corredor=new Categoria($carrera_eleg_corredor->carrera_eleg);
    		$minimoDorsalAsignable=$categoria_corredor->GetMinDorsalAsignable();
    	}
    	//obtengo el maximo dorsal para su carrera_eleg
    	$maxDorsalObject=$db->custom_query("SELECT max(dorsal) maximo FROM ".PagoInscripcion::NOMBRE_TABLA_INSCRITOS." WHERE carrera_eleg=$carrera_eleg_corredor->carrera_eleg");
    	$maxDorsal=intval($maxDorsalObject->maximo);
    	if($maxDorsal< $minimoDorsalAsignable){
    		$maxDorsal=$minimoDorsalAsignable;
    	}else{
    		$maxDorsal=$maxDorsal+1;
    	}
    	$db->update(self::NOMBRE_TABLA_INSCRITOS,array("dorsal"=>$maxDorsal), "id", $NumeroInscripcion);
    
    }
    
    function GetAnoNacimiento(){
    	return $this->FechaNacimiento->format("Y");	 
    }
    
    
    function GetEsInscripcionQueSePaga(){
    	return ($this->importe!=0);
    	/*// No se paga
    	//si es infantil
    	//si es 5km pero a nacido en juveniles
    	 if(	$this->Carrera_eleg==ConfigCarrera::CATEGORIA_INFANTIL ||
    	 		($this->Carrera_eleg==ConfigCarrera::CATEGORIA_5_KM &&
    	 				($this->GetAnoNacimiento()==ConfigCarrera::ANO_JUVENIL1 ||$this->GetAnoNacimiento()==ConfigCarrera::ANO_JUVENIL2 ) 
    	 		)
    	 )
    	 {
    	 	return false;
    	 }else{
    	 	return true;
    	 }*/
    }
    
   

    
  
    
    
    
}


?>