<?php
	

	require_once 'classes/Common.php';
	require_once 'classes/PagoArdoi.php';
	require_once 'classes/PagoInscripcion.php';
	require_once 'classes/Database.php';
	include 'classes/apiredsys/apiRedsys.php';
	include 'classes/TPVConfig.php';
	require_once 'classes/Categoria.php';
	
	session_start();
	$exito=true;
	$pagoInscripcion=$_SESSION["datosInscripcion"];
	
	if(!isset($pagoInscripcion)){
		RedirigirAInicio();
	}
	
	
	
	//la meto en la BD
	$NumeroInscripcion=$pagoInscripcion->insertarInscripcion();
	if($NumeroInscripcion==-1){
		RedirigirAError("No se ha podido guardar la inscripcion en la BD");
		die();
	}
	//no pagan infantiles o juveniles de 5km
	
	if(!$pagoInscripcion->GetEsInscripcionQueSePaga()){
		//no redirigimos al TPV sino que pagamos y asignamos dorsal
		PagoInscripcion::EstablecerPagado($NumeroInscripcion, "autopagada");
		PagoInscripcion::SetDorsal($NumeroInscripcion);
		$_SESSION["pagoInscripcion"]=$pagoInscripcion;
		header("Location:".ConfigCarrera::URL_INSCRIPCION_OK."?Infantil=true" );
		die();
	}
	
	// Se crea Objeto TPV
	$miObj = new RedsysAPI;
	// Valores de entrada
	$fuc=$CONFIG_FUC;//PRE=>159167998 PRO=>"159167998"; //cogido de la consola
	$terminal=$CONFIG_terminal; // cogido del correo
	$moneda=$CONFIG_moneda; //PRE=>000 PRO=> 978 
	$trans=$CONFIG_trans; //0 – Autorización
	$url=TPVConfig::CONFIG_URL_CALLBACK;	//Obligatorio si el comercio tiene notificación “on-line”. URL del comercio que recibirá un post con los datos de la transacción.
	$urlOK=TPVConfig::CONFIG_URL_OK;// "http://carrera.ardoi.es/inscripcion/InscripcionOk.php";
	$urlKO=TPVConfig::CONFIG_URL_KO;
	
	//Opcional: si se envía será utilizado como URLOK ignorando el configurado en el módulo de administración en caso de tenerlo.
	//Opcional: si se envía será utilizado como URLKO ignorando el configurado en el módulo de administración en caso de tenerlo
	$id=$pagoInscripcion->NumeroInscripcion;// ID de la orden , debemos generarla en la BD para meter la inscripccion como NO PAGADA antes
	//obtengo la cuota
	$categoria=new Categoria($pagoInscripcion->Carrera_eleg);
	$amount=$pagoInscripcion->ImporteTPV."";
	$productoDescripcion="Inscripcion Carrera Camino de Santiago - Ardoi";//
	$productoTitular=$pagoInscripcion->Nombre." ".$pagoInscripcion->Apellido1." ".$pagoInscripcion->Apellido2 ;
	$Ds_Merchant_MerchantData=json_encode($pagoInscripcion);
	// Se Rellenan los campos
	
	$miObj->setParameter("DS_MERCHANT_AMOUNT",$amount);
	$miObj->setParameter("DS_MERCHANT_ORDER",strval($id));
	$miObj->setParameter("DS_MERCHANT_MERCHANTCODE",$fuc);
	$miObj->setParameter("DS_MERCHANT_CURRENCY",$moneda);
	$miObj->setParameter("DS_MERCHANT_TRANSACTIONTYPE",$trans);
	$miObj->setParameter("DS_MERCHANT_TERMINAL",$terminal);
	$miObj->setParameter("DS_MERCHANT_MERCHANTURL",$url);
	$miObj->setParameter("DS_MERCHANT_URLOK",$urlOK);
	$miObj->setParameter("DS_MERCHANT_URLKO",$urlKO);
	$miObj->setParameter("Ds_Merchant_ProductDescription",$productoDescripcion);
	$miObj->setParameter("Ds_Merchant_Titular",$productoTitular);
	$miObj->setParameter("Ds_Merchant_SumTotal",$amount);
	$miObj->setParameter("Ds_Merchant_MerchantData",$Ds_Merchant_MerchantData);
	
	
	//Datos de configuración
	$version="HMAC_SHA256_V1";
	$kc =$CLAVE_FIRMA; //'sq7HjrUOBfKmC576ILgskD5srU870gJ7';//PRE=> sq7HjrUOBfKmC576ILgskD5srU870gJ7  PRO=>'Mk9m98IfEblmPfrpsawt7BmxObt98Jev';//Clave recuperada de CANALES
	// Se generan los parámetros de la petición
	$params = $miObj->createMerchantParameters();
	$signature = $miObj->createMerchantSignature($kc);
	
	

	
		
		
		
	
	
	
	
?>

<html>
<head>
 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script language="JavaScript">
		function mandarFormulario(){
			$( "#target" ).submit();
		};
	</script>
</head>

 
<body onLoad="mandarFormulario();">


<form action="<?php echo $URL_PAGO;?>" method="POST"  method="post" id="target">
	<input type="hidden" name="Ds_SignatureVersion" value="<?php echo $version; ?>"/>
	<input type="hidden" name="Ds_MerchantParameters" value="<?php echo $params; ?>"/>
	<input type="hidden" name="Ds_Signature" value="<?php echo $signature; ?>"/>
	
</form>
<!--  se hace un autopost del formulario-->

</body>
</html>
