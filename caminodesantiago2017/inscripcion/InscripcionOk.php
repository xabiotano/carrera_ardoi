<?php
	require_once 'classes/apiredsys/apiRedsys.php';
	require_once 'classes/TPVConfig.php';
	require_once 'classes/Common.php';
	require_once 'classes/MailHelper.php';
	require_once 'classes/ConfigCarrera.php';
			
	session_start();
	
	$infantil=false;
	if(isset($_GET["Infantil"]) && $_GET["Infantil"]=="true"){
		$pagoInscripcionArray=$_SESSION["pagoInscripcion"];
		$infantil=true;
		$pagoInscripcion = new stdClass();
		foreach ($pagoInscripcionArray as $key => $value)
		{
			$pagoInscripcion->$key = $value;
		}
	}else {

		$miObj = new RedsysAPI;
		
		$version = $_GET["Ds_SignatureVersion"];
		$datos = $_GET["Ds_MerchantParameters"];
		$signatureRecibida = $_GET["Ds_Signature"];
		
		if(!isset($version)){
			//RedirigirAInicio();
		}
		$decodec = $miObj->decodeMerchantParameters($datos);
		$kc = $CLAVE_FIRMA;//'sq7HjrUOBfKmC576ILgskD5srU870gJ7';//PRE=> sq7HjrUOBfKmC576ILgskD5srU870gJ7 PRO=> 'Mk9m98IfEblmPfrpsawt7BmxObt98Jev'; //Clave recuperada de CANALES
		$firma = $miObj->createMerchantSignatureNotif($kc,$datos);
		
		if ($firma === $signatureRecibida){
		
		} else {
			die();
		
		}
		
		$CodigoRespuesta=$miObj->getParameter("Ds_Response");
		$Ds_Order=$miObj->getParameter("Ds_Order"); //numero de pedido=> Mismo valor que en la petici�n.
		$Ds_MerchantData=$miObj->getParameter("Ds_MerchantData"); //Datos del comercio Informaci�n opcional enviada por el comercio en el formulario de pago.
		
		$pagoInscripcion=json_decode(urldecode($Ds_MerchantData));
	}
	

	
	$Nombre=$pagoInscripcion->Nombre;
	$Apellido1=$pagoInscripcion->Apellido1;
	$Apellido2=$pagoInscripcion->Apellido2;
	$Email=$pagoInscripcion->Email;
	$Documento=$pagoInscripcion->Documento;
	$Tarifa=$pagoInscripcion->Tarifa;
	$NumeroInscripcion=$pagoInscripcion->NumeroInscripcion;
	
	//para luego imprimirla si quiere
	$_SESSION["IdAtleta"]=$NumeroInscripcion;
	
	//mandamos el email
	if(!isset($_SESSION["mailEnviado_$NumeroInscripcion"]) ){
		MailHelper::SendMail($Email,$NumeroInscripcion);
	}
	
	$_SESSION["mailEnviado_$NumeroInscripcion"]=true;

?>




<html>
<head>
  <title>Inscripción Exitosa</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <link href="../css/bootstrap.min.css" rel="stylesheet">
  	<link href="../css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/ardoiweb.css">
	<link href="../css/main.css" rel="stylesheet">
	<link href="../css/animate.css" rel="stylesheet">	
	<link href="../css/responsive.css" rel="stylesheet">
  
  
  
</head>
<body>
	<header id="header" role="banner">		
		<div class="main-nav">
			<div class="container">
				
		        <div class="row">	        		
		            <div class="navbar-header">
		                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		                    <span class="sr-only">Toggle navigation</span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
		               <!--  <a class="navbar-brand" href="index.html">
		                	<img class="img-responsive" src="images/logo.png" alt="logo" id="logo_ardoi" style="width:90px;height:90px;margin-top:30px;">
		                </a> -->
		                 <a class="navbar-brand" rel="home" href="#" title="Buy Sell Rent Everyting">
					        <img  src="../images/logo.png"/>
					    </a>                   
		            </div>
		            <?php include_once '../componentes/navbar.php';?>
		        </div>
	        </div>
        </div>                    
    </header>
    


<div class="container">
	<br/>
    <br/>
	<br/>
	<br/>
    <br/>
	<br/>
	<ul class="nav nav-pills">
		   <li role="presentation"  disabled="active"><button type="button" class="btn btn-lg btn-primary" disabled="disabled">Inscripción completada</button></li>
	</ul>
	<div class="panel panel-default">
  		<div class="panel-heading">Inscripciones para la XXIV Carrera Camino de Santiago</div>
  		
  		<div class="panel-body">

	    
		<div class="panel panel-success">
			<div class="panel-heading">
		    <h3 class="panel-title">¡Enhorabuena! Se ha inscrito con éxito . Se le acaba de mandar un email con el justificante de la inscripción.</h3>
		  </div>
		  <div class="panel-body">
		    <label for="DiaCarrera">Evento</label>
		    <input type="text" name="DiaCarrera" class="form-control" placeholder="Readonly input" readonly value="Carrera Camino de Santiago - 2 Abril 2017" />
			
		 	<label for="NombreCompleto">Nombre Completo</label>
			<input type="text" name="NombreCompleto" class="form-control" placeholder="Readonly input" readonly value="<?php echo $Nombre." ".$Apellido1." ".$Apellido2;?>" />
			<?php if(!$infantil){
					//sino es infantil muestro el NIF y la distancia de la carrera
				?>
				<label for="Documento">NIF</label>
				<input type="text" name="Documento" class="form-control" placeholder="Readonly input" readonly value="<?php echo $Documento;?>" />
				<label for="Tarifa">Distancia Carrera</label>
				<input type="text" name="Tarifa" class="form-control" placeholder="Readonly input" readonly value="<?php echo ($Tarifa==5)? "Carrera 5Km":"Carrera 10Km" ?>" />
			
			<?php 
			}?>
			<label for="Email">Email</label>
			<input type="text" name="Email" class="form-control" placeholder="Readonly input" readonly value="<?php echo $Email;?>" />
			
			
		
		</div>
	<br/>
	<div class="col-md-5">
		<br/>
		<a href="ImprimirJustificante.php" target="_blank"   type="button" class="btn btn-default">
  			<span class="glyphicon glyphicon-print" ></span>Imprimir Justificante
		</a>
		
		<a href="<?php echo ConfigCarrera::URL_INSCRITOS;?>" target="_blank"   type="button" class="btn btn-default">
  			<span class="glyphicon glyphicon-list-alt" ></span>Lista de Inscritos
		</a>
		
		<a href="<?php echo ConfigCarrera::URL_INFORMACION_CARRERA;?>" target="_blank"   type="button" class="btn btn-default">
  			<span class="glyphicon glyphicon-file" ></span>Informacion de la carrera
		</a>
		
	</div>
	<br/>

	
	
	
	
	
	</div>
	<br/>
	<br/>
	</div>
	
</div>
</body>
</html>