<?php
	include 'classes/apiredsys/apiRedsys.php';
	include 'classes/TPVConfig.php';
	include 'classes/Common.php';
	
	// Se crea Objeto
	$miObj = new RedsysAPI;

	

	$version = $_GET["Ds_SignatureVersion"];
	$datos = $_GET["Ds_MerchantParameters"];
	$signatureRecibida = $_GET["Ds_Signature"];
	
	if(!isset($version)){
		RedirigirAInicio();
	}
	
	$decodec = $miObj->decodeMerchantParameters($datos);
	$kc = $CLAVE_FIRMA;//'sq7HjrUOBfKmC576ILgskD5srU870gJ7';//PRE=> sq7HjrUOBfKmC576ILgskD5srU870gJ7 PRO=> 'Mk9m98IfEblmPfrpsawt7BmxObt98Jev'; //Clave recuperada de CANALES
	$firma = $miObj->createMerchantSignatureNotif($kc,$datos);
	
	if ($firma === $signatureRecibida){
		
	} else {
		die();
	
	}
	
	$CodigoRespuesta=$miObj->getParameter("Ds_Response");
	$Ds_Order=$miObj->getParameter("Ds_Order"); //numero de pedido=> Mismo valor que en la petici�n.
	$Ds_MerchantData=$miObj->getParameter("Ds_MerchantData"); //Datos del comercio Informaci�n opcional enviada por el comercio en el formulario de pago.
	
	
	$json=json_decode(urldecode($Ds_MerchantData));
	//print_r($json);
	//RMA OKstdClass Object ( [Dorsal] => [Tarifa] => 5 [NumeroInscripcion] => 000050 [Nombre] => xabier [Apellido1] => otano [Apellido2] => andres [Genero] => Hombre [FechaNacimiento] => 1948-04-22 [Documento] => 72806038k [Email] => xabi.otano@gmail.com [Movil] => 617331770 [Poblacion] => Pamplona [Provincia] => Navarra [Club] => Ardoi [ViveEnZizur] => false [TallaCamiseta] => S [importe] => 0 )
	
	
	$Nombre=$json->Nombre;
	$Apellido1=$json->Apellido1;
	$Apellido2=$json->Apellido2;
	$Email=$json->Email;
	$Documento=$json->Documento;
	$Tarifa=$json->Tarifa;
	

?>




<html>
<head>
  <title>Inscripción Fallida</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/ardoiweb.css">

  
</head>
<body>
<div class="container">
	
	<div class="page-header">
		<img src="images/ardoi_logo.png" alt="" class="img-thumbnail">
	  <h1>Inscripciones para la XXIV Carrera Camino de Santiago<small></small></h1>
	</div>
	
	<ul class="nav nav-pills">

	    <li role="presentation"  disabled="active"><button type="button" class="btn btn-lg btn-primary" disabled="disabled">Inscripción no completada</button></li>
	</ul>
	
	<div class="panel panel-default">
	
	  	<div class="panel-body">
	    
	    <ol class="breadcrumb">
			  <li><a href="#">Detalles Carrera</a></li>
			  <li class="active">Pago fallido</li>
		</ol>
	    
	    
		<div class="panel panel-danger">
			<div class="panel-heading">
		    <h3 class="panel-title">¡Oups! Ocurrió un error durante su inscripción</h3>
		  </div>
		  
	
	<div class="col-md-5">
		<br/>
		<a  href="" type="button" class="btn btn-default">
  			<span class="glyphicon glyphicon-repeat" ></span> Volver a intentarlo
		</a>
		<br/>
		<br/>
		<a href="" type="button" class="btn btn-default">
  			<span class="glyphicon glyphicon-envelope" ></span> Contactar en ardoiatletismo@gmail.com
		</a>
		<br/>
	<br/>
	<br/>
	<br/>
		
	</div>
	
	</div>
		
	
		
	
		
		
		
	
	
	<br/>
	<br/>
	</div>
	
</div>
</body>
</html>