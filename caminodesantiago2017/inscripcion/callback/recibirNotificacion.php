<?php

	include '../classes/apiredsys/apiRedsys.php';
	include '../classes/TPVConfig.php';
	require_once '../classes/PagoArdoi.php';
	require_once '../classes/PagoInscripcion.php';
	require_once '../classes/Database.php';
	require_once '../classes/HistoricoPago.php';
	require_once '../classes/AtletaHelper.php';
	
	// Se crea Objeto
	$miObj = new RedsysAPI;

	
	
	
	
	if (!empty( $_POST ) ) {//URL DE RESP. ONLINE
				
					$historicoPago=new HistoricoPago();
					
	
					$version = $_POST["Ds_SignatureVersion"];
					$datos = $_POST["Ds_MerchantParameters"];
					$signatureRecibida = $_POST["Ds_Signature"];
					

					$decodec = $miObj->decodeMerchantParameters($datos);	
					$kc = $CLAVE_FIRMA;
					$firma = $miObj->createMerchantSignatureNotif($kc,$datos);	

					if ($firma === $signatureRecibida){
						echo "FIRMA OK";
					} else {
						$historicoPago=new HistoricoPago();
						$historicoPago->InsertarHistoricoPago(array("Ds_Date"=>time()));
						die();
						
					}
					
					//leo los parametros de la notificacion
					$CodigoRespuesta=$miObj->getParameter("Ds_Response");
					$Ds_Date=$miObj->getParameter("Ds_Date"); //Fecha de la transacci�n
					$Ds_Hour=$miObj->getParameter("Ds_Hour"); //Hora de la transacci�n
					$Ds_Amount=$miObj->getParameter("Ds_Amount");//Importe Mismo valor que en la petici�n
					$Ds_MerchantCode=$miObj->getParameter("Ds_MerchantCode");// Identificaci�n de comercio: c�digo FUCMismo valor que en la petici�n. 
					$Ds_Order=$miObj->getParameter("Ds_Order"); //numero de pedido=> Mismo valor que en la petici�n.
					$Ds_MerchantData=$miObj->getParameter("Ds_MerchantData"); //Datos del comercio Informaci�n opcional enviada por el comercio en el formulario de pago.
					$Ds_SecurePayment=$miObj->getParameter("Ds_SecurePayment"); //Pago Seguro 0 � Si el pago NO es seguro 1 � Si el pago es seguro
					$Ds_Card_Country=$miObj->getParameter("Ds_Card_Country");//Pa�s del titular
					$Ds_AuthorisationCode=$miObj->getParameter("Ds_AuthorisationCode");//C�digo de autorizaci�n
					$Ds_Card_Type=$miObj->getParameter("Ds_Card_Type");//Tipo de Tarjeta C � Cr�dito  D - D�bito
					
					$resultado=PagoArdoi::getInfoDsResponse($CodigoRespuesta);
					$resultadoTexto=$resultado["resultadoText"];
					
				
					
					try{
						$insertar=array();
						$insertar["Ds_Order"]=intval($Ds_Order);
						$insertar["Ds_Response"]=$CodigoRespuesta;
						$insertar["Ds_Date"]= date('Y-d-m', strtotime(str_replace('-', '/', urldecode($Ds_Date))));						$insertar["Ds_Hour"]=urldecode($Ds_Hour);
						$insertar["Ds_Amount"]=$Ds_Amount;
						$insertar["Ds_MerchantCode"]=$Ds_MerchantCode;
						$insertar["Ds_MerchantData"]=(isset($Ds_MerchantData)) ? urldecode($Ds_MerchantData):"";
						$insertar["Ds_SecurePayment"]=$Ds_SecurePayment;
						$insertar["Ds_Card_Country"]=$Ds_Card_Country;
						$insertar["Ds_AuthorisationCode"]=$Ds_AuthorisationCode;
						$insertar["Ds_Card_Type"]=(isset($Ds_Card_Type)) ? $Ds_Card_Type:"";
						$insertar["Ds_Response_Texto"]=$resultadoTexto;
						$IdHistoricoPago=$historicoPago->InsertarHistoricoPago($insertar);
					}
					catch(Exception $e){
						$IdHistoricoPago=$historicoPago->InsertarHistoricoPago(array("Ds_Response_Texto"=>$e));
							
					}
					/*
					{
						1"Ds_Order": 608,
						2"Ds_Response": null,
						3"Ds_Date": "03%2F04%2F2016",
						4"Ds_Hour": "21%3A17",
						5"Ds_Amount": "1250",
						6"Ds_MerchantCode": "159167998",
						7"Ds_MerchantData": null,
						8"Ds_SecurePayment": "1",
						9"Ds_Card_Country": "724",
						10"Ds_AuthorisationCode": "362669",
						11"Ds_Card_Type": null,
						12"Ds_Response_Texto": "Transacci\u00f3n autorizada para pagos y preautorizaciones"
					}
					
				*/
					if($IdHistoricoPago==-1){
						echo "ERROR IdHistoricoPago=-1";
						//nose que hacer aqui??
					}
					
					//lo guardo en la BD en el HistoricoPagos  PK Ds_Order
					if(!$resultado["resultadoExito"]){
						//pongo la tabla inscripciones como pago FAILED buscando por  NumeroInscripcion==$Ds_Order
						PagoInscripcion::EstablecerPagoFallido($Ds_Order,$IdHistoricoPago);//le asigno el ID del historico pago
						
					}else{
						//pongo en la tabla inscripciones como pago OK buscando por  NumeroInscripcion==Ds_MerchantCode
						PagoInscripcion::EstablecerPagado($Ds_Order,$IdHistoricoPago);//le asigno el ID del historico pago
						PagoInscripcion::SetDorsal($Ds_Order);//le asignamos el dorsal
						MailHelper::SendMail($Email,$Ds_Order);
					}
					
					
	}
	

?>
