<?php
	require_once 'classes/PagoInscripcion.php';
	require_once 'classes/ConfigCarrera.php';
	require_once 'classes/Carrera.php';
	require_once 'classes/Categoria.php';
	
	
	$carrera=new Carrera(ConfigCarrera::ID_CARRERA);
	
?>




<html>
<head>
  <title>Inscripcion</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  	<link href="../css/bootstrap.min.css" rel="stylesheet">
  	<link href="../css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/ardoiweb.css">
	<link href="../css/main.css" rel="stylesheet">
	<link href="../css/animate.css" rel="stylesheet">	
	<link href="../css/responsive.css" rel="stylesheet">
</head>

<body>

	<header id="header" role="banner">		
		<div class="main-nav">
			<div class="container">
				
		        <div class="row">	        		
		            <div class="navbar-header">
		                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		                    <span class="sr-only">Toggle navigation</span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
		               <!--  <a class="navbar-brand" href="index.html">
		                	<img class="img-responsive" src="images/logo.png" alt="logo" id="logo_ardoi" style="width:90px;height:90px;margin-top:30px;">
		                </a> -->
		                 <a class="navbar-brand" rel="home" href="#" title="Buy Sell Rent Everyting">
					        <img  src="../images/logo.png"/>
					    </a>                   
		            </div>
		            <?php include_once '../componentes/navbar.php';?>
		        </div>
	        </div>
        </div>                    
    </header>




<div class="container">
	<br/>
    <br/>
	<br/>
	<br/>
    <br/>
	<br/>
	<ul class="nav nav-pills">
		  <li role="presentation"  class="active" ><button type="button" class="btn btn-lg btn-primary" disabled="disabled">Selecciona distancia</button></li>
		  <li role="presentation"  disabled="disabled" ><button type="button" class="btn btn-lg btn-default" disabled="disabled">Rellena tus datos</button></li>
		  <li role="presentation"  disabled="disabled"><button type="button" class="btn btn-lg btn-default" disabled="disabled">Pago</button></li>
	</ul>
	<div class="panel panel-default">
  		<div class="panel-heading">Inscripciones en la carrera</div>
  		
  		<div class="panel-body">
			<?php if($carrera->SePuedeInscribir()){?>
			<div class="jumbotron">
			<a href="<?php echo ConfigCarrera::URL_CARRERA;?>">Volver</a>
			<h2>Lista de Precios</h2>
 			 <p>Lista de precios para cada categoría</p>  
			 <table class="table table-bordered">
			    <thead>
			      <tr>
			         <th>Cuota inscripción</th>
			         <th colspan="2"></th>
			      </tr>
			    </thead>
			    <tbody>
			      <tr>
			        <td>Hasta Juveniles</td>
			        <td  colspan="2">Alimentos NO perecederos</td>
			      </tr>
			      <tr>
			        <td rowspan="2">Junior en adelante</td>
			        <td>
			           5 km 
			       	 </td>
			       	 <td>
			        	8€
			       	 </td>
			      </tr>
			      <tr>
			        <td>
			           10 km 
			       	 </td>
			       	 <td>
			        10€
			       	 </td>
			      </tr>
			     
			    </tbody>
			  </table>
			
			</div>
			<div class="jumbotron">
		  		<h1>Carreras Infantiles <h3>(hasta Cadete)</h3>  </h1>
		  		<p>Para los nacidos en el 2002 y posteriores </p>
		  		<p><a class="btn btn-success btn-lg glyphicon glyphicon-fire" href="rellenarDatos.php?tipoInscripcion=I" role="button"> Inscribirme</a></p>
			</div>
	    	<div class="jumbotron">
		  		<h1>Carrera de 5km <h3>(Absoluto+ Juveniles)</h3></h1>
		  		<p>Para nacidos en el año 2001 y anteriores</p>
		  		<p><a class="btn btn-success btn-lg glyphicon glyphicon-fire" href="rellenarDatos.php?tipoInscripcion=5" role="button"> Inscribirme</a></p>
			</div>

			<div class="jumbotron">
		  		<h1>Carrera de 10km <h3>(Absoluto)</h3> </h1>
		  		<p>Para nacidos en el año 1999 y anteriores</p>
		  		<p><a class="btn btn-success btn-lg glyphicon glyphicon-fire" href="rellenarDatos.php?tipoInscripcion=10" role="button"> Inscribirme</a></p>
			</div>
	 	<?php 
	 	
				}
				else{
					?>
					<div class="jumbotron">
						<p >Lo sentimos. El plazo de inscripción por internet ha terminado</p>
						<a href="carrera.ardoi.es">Volver</a>
						<br/>
						<br/>
						<br/>
						<br/>
						<br/>
						<br/>
					</div>
					
					<?php 
				}
	 	?>
			</div>
		</div>
	</div>
</body>
</html>