<?php
	require_once 'classes/ConfigCarrera.php';

	session_start();
	
	if(!isset($_SESSION["IdAtleta"])){
			echo "ERROR";
			die();
	}else
	{
		$id_atleta=intval($_SESSION["IdAtleta"]);
	}
	
	
	
	$url = ConfigCarrera::URL_WS_IMPRESION.$id_atleta;
	$data = curl_get_contents($url);
	header("Content-type: application/octet-stream");
	header("Content-disposition: attachment;filename=JUSTIFICANTE-".$id_atleta.".pdf");
	echo $data;
		



function curl_get_contents($url)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_URL, $url);
	$data = curl_exec($ch);
	curl_close($ch);
	return $data;
}

?>
