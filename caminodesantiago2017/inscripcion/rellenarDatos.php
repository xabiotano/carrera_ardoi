<?php
	require_once 'classes/Common.php';
	require_once 'classes/PagoArdoi.php';
	require_once 'classes/PagoInscripcion.php';
	require_once 'classes/ConfigCarrera.php';	
	require_once 'classes/Carrera.php';
	require_once 'classes/Club.php';


	
	session_start();	
	//borro los datos de la sesion
	unset($_SESSION["datosInscripcion"]);
	
	if(isset($_GET["tipoInscripcion"]))
	{
		$tipoInscripcion= $_GET["tipoInscripcion"];
		if($tipoInscripcion!=5 && $tipoInscripcion!=10 && $tipoInscripcion!='I'){
			header("Location:elegirCarrera.php");
			die();
		}else{
			$_SESSION['tipoInscripcion'] = $tipoInscripcion;
		}
	}else{
		if(!isset($_SESSION['tipoInscripcion'])){
			header("Location:elegirCarrera.php");
			die();
		}
		$tipoInscripcion=$_SESSION['tipoInscripcion'];
	}
	
	if($tipoInscripcion==5){
		$textoModo="5 KM";
	}else if($tipoInscripcion==10){
		$textoModo="10 KM";
	}else if($tipoInscripcion=="I"){
		$textoModo="Categorías Infantiles";
	}
	
	
	
	$ErroresFormulario=array();
	if (strtoupper($_SERVER['REQUEST_METHOD']) == 'POST'){
		//estamos en un POST
		
	 	$Tarifa=$tipoInscripcion;//vale 5 o 10 o I segun la carrera
	    $Nombre=$_POST['Nombre'];
		$Apellido1=$_POST['Apellido1'];
		$Apellido2=$_POST['Apellido2'];
		$Genero=$_POST['Genero'];
		$FechaNacimiento=$_POST['FechaNacimiento'];
		$Documento=cleanNif($_POST['Documento']);
		$Email=$_POST['Email'];
		$Email2=$_POST['Email2'];
		$Movil=$_POST['Movil'];
		$Poblacion=$_POST['Poblacion'];
		$Provincia=$_POST['Provincia'];
		$Club=$_POST['Club'];
		$ViveEnZizur=(isset($_POST['ViveEnZizur']) && $_POST['ViveEnZizur']==true) ? "local":"";
		$TallaCamiseta="-1";//$_POST['TallaCamiseta'];
		
		
		$ErroresFormulario["Nombre"]=comprobarCampo($Nombre,2,50,"texto","Nombre");
		$ErroresFormulario["Apellido1"]=comprobarCampo($Apellido1,2,50,"texto","Apellido1");
		$ErroresFormulario["Apellido2"]=comprobarCampo($Apellido2,2,50,"texto","Apellido2");
		$ErroresFormulario["Genero"]=comprobarCampo($Genero,1,1,"texto","Genero");
		$ErroresFormulario["FechaNacimiento"]= comprobarCampo($FechaNacimiento,10,10,"texto","Fecha de Nacimiento");
		$ErroresFormulario["Documento"]=comprobarCampo($Documento,9,9,"texto","NIF");
		$ErroresFormulario["Email"]=comprobarEmails($Email,$Email2);
		$ErroresFormulario["Movil"]=comprobarCampo($Movil,9,9,"numerico","Movil");
		$ErroresFormulario["Poblacion"]=comprobarCampo($Poblacion,2,50,"texto","Poblacion");
		$ErroresFormulario["Provincia"]=comprobarCampo($Provincia,2,50,"texto","Provincia");
		$ErroresFormulario["Club"]=comprobarCampo($Club,2,50,"texto","Club");
		$ErroresFormulario["ViveEnZizur"]="";
		//$ErroresFormulario["TallaCamiseta"]=comprobarCampo($TallaCamiseta,1,1,"texto","Talla de Camiseta");
		
		//sino tiene otros errores compruebo la validez del NIF
		if(!isset($ErroresFormulario["Documento"])){
			if(!validar_dni($Documento)){
				$ErroresFormulario["Documento"]="Introduce un NIF válido";
			}
		}
		
		if(!isset($ErroresFormulario["FechaNacimiento"])){
			if(!validateDate($FechaNacimiento)){
				$ErroresFormulario["FechaNacimiento"]="La fecha es no válida";
			}
		}
		
		$exitovalidarCarreraValida=validarCarreraValida($tipoInscripcion,$FechaNacimiento );
		if(!$exitovalidarCarreraValida["exito"]){
			$ErroresFormulario["FechaNacimiento"]=$exitovalidarCarreraValida["textoError"];
		}
		
		if(countErrors()==0){
			$pagoInscripcion= new PagoInscripcion(	
													$Tarifa,
													$Nombre,
													$Apellido1,
													$Apellido2, 
													$Genero,
													$FechaNacimiento,
													$Documento, 
													$Email, 
													$Movil, 
													$Poblacion,
													$Provincia,
													$Club, 
													$ViveEnZizur,
													$TallaCamiseta);
			//print_r($pagoInscripcion);
			$_SESSION["datosInscripcion"]=$pagoInscripcion;
			header("Location:pagarInscripcion.php");
			die();
		}
	}
	
	
	
	
	
	
		
	
	
	//comprobaciones

	function cleanNif($nif){
		$nueva_cadena = ereg_replace("[^A-Za-z0-9]", "", $nif);
		return $nueva_cadena;
	}
	function comprobarEmails($Email1,$Email2){
		if($Email1==$Email2){
			if (!filter_var($Email1, FILTER_VALIDATE_EMAIL)) {
				return "El email introducido no es válido";
			}	
		}else{
			return "Los emails no coinciden";	
		}
		
	}
	
	function comprobarCampo($campo, $longitudMinima,$longitudMax,$tipo, $NombreCampo){
		if(isset($campo) && !empty($campo)){
			if(strlen($campo)<$longitudMinima){
				return "$NombreCampo debe tener al menos $longitudMinima caracteres";
			}
			if(strlen($campo)>$longitudMax){
				return "$NombreCampo no debe tener más de $longitudMax caracteres";
			}
			if($tipo=="numerico"){
				if(!is_numeric($campo)){
					return "$NombreCampo debe ser númerico";
				}
			}
			
		}else{
			return "$NombreCampo está vacío";
		}
	}
	
	
	
	
	function validar_dni($dni){
			if(strlen($dni)<9) {
				return "DNI demasiado corto.";
			}
		 
			$dni = strtoupper($dni);
		 
			$letra = substr($dni, -1, 1);
			$numero = substr($dni, 0, 8);
		 
			// Si es un NIE hay que cambiar la primera letra por 0, 1 ó 2 dependiendo de si es X, Y o Z.
			$numero = str_replace(array('X', 'Y', 'Z'), array(0, 1, 2), $numero);	
		 
			$modulo = $numero % 23;
			$letras_validas = "TRWAGMYFPDXBNJZSQVHLCKE";
			$letra_correcta = substr($letras_validas, $modulo, 1);
		 
			if($letra_correcta!=$letra) {
				return false;
			}
			else {
				return true;
			}
	}
	
	
	function validateDate($date, $format = 'Y-m-d')
	{
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}
	
	
	function validarCarreraValida($tipoInscripcion,$fecha_nac ){
		$devolver=array();
		$devolver["exito"]=true;
		
		if(!isset($fecha_nac) || strlen($fecha_nac)<6  ){
			$devolver["exito"]=false;
			$devolver["textoError"]="La fecha de nacimiento no es válida.";
			return $devolver;
		}
		try{
			
			$fecha_nac = DateTime::createFromFormat('Y-m-d', $fecha_nac);
			$carrera=new Carrera(ConfigCarrera::ID_CARRERA);
			if($tipoInscripcion==5 ){
				//si eres absoluto tu ano de nacimiento no puede ser mayor(mas joven) al absoluto

				if($fecha_nac->format('Y') > ($carrera->primerAnoAbsoluto +2)){
					
					$devolver["exito"]=false;
					$devolver["textoError"]="Solo los nacidos antes de". ($carrera->primerAnoAbsoluto+2) ." pueden participar en una carrera absoluta de 5km";
					return $devolver;
				}
				
			}
			else if( $tipoInscripcion==10){
				//si eres absoluto tu ano de nacimiento no puede ser mayor(mas joven) al absoluto
				if($fecha_nac->format('Y')> $carrera->primerAnoAbsoluto){
					$devolver["exito"]=false;
					$devolver["textoError"]="Solo los nacidos antes de $carrera->primerAnoAbsoluto pueden participar en una carrera absoluta de 10km";
					return $devolver;
				}
			}
			else if($tipoInscripcion=='I'){
				//si eres infantil tu ano de nacimiento no puede ser menor(mas viejo) o igual al absoluto
				
				if(intval($fecha_nac->format('Y'))<= intval($carrera->primerAnoAbsoluto)){
					$devolver["exito"]=false;
					$devolver["textoError"]="Solo los nacidos despues de $carrera->primerAnoAbsoluto pueden participar en una carrera Infantil";
					return $devolver;
				}
			}
			
			return $devolver;
		}catch(Exception $e){
			$devolver["exito"]=false;
			$devolver["textoError"]="Rellene la fecha de nacimiento por favor.";
		}
		return $devolver;
	}
	
	function contieneError($NombreCampo){
		global $ErroresFormulario;
		return !empty($ErroresFormulario[$NombreCampo]);
		
	}
	
	function getError($NombreCampo){
		global $ErroresFormulario;
		return $ErroresFormulario[$NombreCampo];
	
	}
	
	function countErrors(){
		global $ErroresFormulario;
		$numero=0;
		foreach ($ErroresFormulario as $valor){
			if(isset($valor) && strlen($valor)>0){
				$numero++;
				
			}
		}
		return $numero;
	}
	
	

	 	
	
	
	
	
?>
<html>
<head>
  <title>Inscripción</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  	 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  	<link href="../css/bootstrap.min.css" rel="stylesheet">
  	<link href="../css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/ardoiweb.css">
	<link href="../css/main.css" rel="stylesheet">
	<link href="../css/animate.css" rel="stylesheet">	
	<link href="../css/responsive.css" rel="stylesheet">
  
  
  <script>
  
  	$(function() {
   		 $( "#FechaNacimiento" ).datepicker({
   	   		 	changeMonth: true,
   	      		changeYear: true,
   	      		changeDay: true,
   	     		<?php 
   	     		$carrera=new Carrera(ConfigCarrera::ID_CARRERA);
   	     		if($tipoInscripcion!="I"){
   	     			
   	     			$year=$carrera->primerAnoAbsoluto;
   	     			if($tipoInscripcion==5){
   	     				$year=$year+2;
   	     			}
   	     				
   	     			?>
   	     			yearRange: '1920:<?php echo $year?>',
   	     			defaultDate: new Date('1 January 1945'),
   	     		
   	     			<?php 
				}
   	     		else{
   	     			
   	     			?>
   	     				yearRange: '<?php echo (intval($carrera->primerAnoAbsoluto)+3)?>:2015',
   	     				defaultDate: new Date('1 January <?php echo (intval($carrera->primerAnoAbsoluto)+3);?>'),
   	     			<?php
   	     		}
   	     		?>
   	     	 	maxDate: "+0D",
   	     		
   	     		
   	     	 	dateFormat:"yy-mm-dd"});

		   		$('#datos_form').submit(function() {
		   			var myDate = $( "#FechaNacimiento" ).val();
		   			var parsedDate = $.datepicker.parseDate('yy-mm-dd', myDate);
		   			var ano=parsedDate.getFullYear();
		   			var tipoInscripcion=$("#tipoInscripcion").val();
		   			if((ano==2001 || ano==2000) && tipoInscripcion==5){
		   				var c = confirm("Vas a inscribirte como juvenil en 5Km. ¿Deseas continuar?");
		   				 return c;
		   			}
		   			
			   		
		   		    
		   		   
		   		});
// Traducción al español         /////////////////////////////////////////////////
  $(function($){
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
});
		 ////////////////////////////////////////////////////////////  		
		   		var availableTags = [
						<?php 
						$clubs=Club::GetClubs();
						$json="";
						foreach ($clubs as $valor){
							$json=$json."'$valor->club',";
						}
						echo  substr($json, 0, -1);
						?>
				];
						
		   		
   		         $( "#Club" ).autocomplete({
   		              source: availableTags
   		        });



		   		
	   		
    		
   	
  	});
  	
  	window.onload = function() {
  		 var myInput = document.getElementById('Email2');
  		 myInput.onpaste = function(e) {
  		   e.preventDefault();
  		 }
  		}
  	
  </script>
  
</head>
<body>

<header id="header" role="banner">		
		<div class="main-nav">
			<div class="container">
				
		        <div class="row">	        		
		            <div class="navbar-header">
		                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		                    <span class="sr-only">Toggle navigation</span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
		               <!--  <a class="navbar-brand" href="index.html">
		                	<img class="img-responsive" src="images/logo.png" alt="logo" id="logo_ardoi" style="width:90px;height:90px;margin-top:30px;">
		                </a> -->
		                 <a class="navbar-brand" rel="home" href="#" title="Buy Sell Rent Everyting">
					        <img  src="../images/logo.png"/>
					    </a>                   
		            </div>
		            <?php include_once '../componentes/navbar.php';?>
		        </div>
	        </div>
        </div>                    
    </header>

<div class="container">
	
	<br/>
    <br/>
	<br/>
	<br/>
    <br/>
	<br/>
	
	<ul class="nav nav-pills">
	  <li role="presentation"  class="disabled" ><button type="button" class="btn btn-lg btn-default" disabled="disabled">Selecciona distancia</button></li>
	  <li role="presentation"  disabled="active" ><button type="button" class="btn btn-lg btn-primary" disabled="disabled">Rellena tus datos</button></li>
	  <li role="presentation"  disabled="disabled"><button type="button" class="btn btn-lg btn-default" disabled="disabled">Pago</button></li>
	</ul>
	
	<div class="panel panel-default">
	
	  	<div class="panel-body">
	    <div class="alert alert-info" role="alert">Has elegido la carrera de <h4><?php echo $textoModo;?></h4> <br/><a href="elegirCarrera.php" class="alert-link"> Si deseas cambiar la distancia clicka aquí</a></div>
			<div class="col-md-6">
			<form action="rellenarDatos.php" method="post"  role="form" id="datos_form">

			<input type="hidden" id="tipoInscripcion" value="<?php echo $tipoInscripcion;?>" />
			<!--Dorsal: <input type="text" name="name">-->
			<!--Tarifa:--> 
			<!--NumeroInscripcion:-->
				<div class="form-group <?php echo contieneError("Nombre")? "has-error":"";?>" >
					<label for="Nombre" >Nombre:</label>
					<input type="text" class="form-control"  name="Nombre" id="Nombre" value="<?php echo isset($Nombre) ? $Nombre:""?>">
					<?php  echo contieneError("Nombre")? "<span id='helpBlock2' class='help-block'>".getError("Nombre")."</span>":"";?>
					
				</div>
				<div class="form-group <?php echo contieneError("Apellido1")? "has-error":"";?>">
					<label for="Apellido1">Apellido1:</label>
					<input type="text" class="form-control" name="Apellido1" id="Apellido1" value="<?php echo isset($Apellido1) ? $Apellido1:""?>">
					<?php  echo contieneError("Apellido1")? "<span id='helpBlock2' class='help-block'>".getError("Apellido1")."</span>":"";?>
				</div>
				<div class="form-group  <?php echo contieneError("Apellido2")? "has-error":"";?>">
					<label for="Apellido2">Apellido2:</label>
					<input type="text" class="form-control" name="Apellido2" id="Apellido2" value="<?php echo isset($Apellido2) ? $Apellido2:""?>">
				<?php  echo contieneError("Apellido2")? "<span id='helpBlock2' class='help-block'>".getError("Apellido2")."</span>":"";?>
				</div>
				
				<div class="form-group  <?php echo contieneError("Genero")? "has-error":"";?>">
					<label for="Genero">Genero</label>
					<select name="Genero" class="form-control">
						  <option value="">---</option>
						  <option value="M" <?php echo (isset($Genero) && $Genero=="M") ?  "selected" :""?>>Masculino</option>
						  <option value="F" <?php echo  (isset($Genero) && $Genero=="F")?  "selected" :""?>>Femenino</option>
					</select> 
					<?php  echo contieneError("Genero")? "<span id='helpBlock2' class='help-block'>".getError("Genero")."</span>":"";?>
				</div>
				<div class="form-group  <?php echo contieneError("FechaNacimiento")? "has-error":"";?>">
					<label for="FechaNacimiento">Fecha de Nacimiento</label>
					<input type="text" name="FechaNacimiento" id="FechaNacimiento" class="form-control" value="<?php echo isset($FechaNacimiento) ? $FechaNacimiento:""?>">
					<?php  echo contieneError("FechaNacimiento")? "<span id='helpBlock2' class='help-block'>".getError("FechaNacimiento")."</span>":"";?>
				</div>
				<?php if($tipoInscripcion=="I"){
					?>
					<div class="row">
						<div class="col-md-12">
						
							<span class="">*Has elegido la carrera de <?php echo $textoModo;?> Por eso las fechas de nacimiento estan dentro de un rango limitado</span>
						
						</div>
					</div>
					<br/>
					<br/>
			
					<?php 
				}
				?>
				<?php 
				//si es infantil no hay documento
				if($tipoInscripcion!="I"){
					?>
					<div class="form-group  <?php echo contieneError("Documento")? "has-error":"";?>">
					<label for="Documento">NIF</label>
					<input type="text" name="Documento"  class="form-control"  value="<?php echo isset($Documento) ? $Documento:""?>">
					<?php  echo contieneError("Documento")? "<span id='helpBlock2' class='help-block'>".getError("Documento")."</span>":"";?>
				</div>
					<?php 
				}
				else{
					//le asigno un NIF por defecto
					?>
						<input type="hidden" name="Documento"  class="form-control"  value="<?php echo ConfigCarrera::NIF_INFANTIL;?>">
					<?php
				}
					
				?>
				
				<div class="form-group  <?php echo contieneError("Email")? "has-error":"";?>">
					<label for="Email">Email</label>
					<input type="email" name="Email" class="form-control" value="<?php echo isset($Email) ? $Email:""?>">
					<?php  echo contieneError("Email")? "<span id='helpBlock2' class='help-block'>".getError("Email")."</span>":"";?>
				</div>
				<div class="form-group  <?php echo contieneError("Email2")? "has-error":"";?>">
					<label for="Email2">Confirma Email</label>
					<input type="email" name="Email2" id="Email2" class="form-control" value="<?php echo isset($Email2) ? $Email2:""?>">
					<?php  echo contieneError("Email2")? "<span id='helpBlock2' class='help-block'>".getError("Email2")."</span>":"";?>
				</div>
				<div class="form-group  <?php echo contieneError("Movil")? "has-error":"";?>">
					<label for="Movil">Telefono movil (sin espacios ni extensiones)</label>
					<input type="text" name="Movil" class="form-control" value="<?php echo isset($Movil) ? $Movil:""?>">
					<?php  echo contieneError("Movil")? "<span id='helpBlock2' class='help-block'>".getError("Movil")."</span>":"";?>
				</div>
				<div class="form-group  <?php echo contieneError("Poblacion")? "has-error":"";?>">
					<label for="Poblacion">Poblacion</label>
					<input type="text" name="Poblacion" class="form-control" value="<?php echo isset($Poblacion) ? $Poblacion:""?>">
					<?php  echo contieneError("Poblacion")? "<span id='helpBlock2' class='help-block'>".getError("Poblacion")."</span>":"";?>
				</div>
				<div class="form-group  <?php echo contieneError("Provincia")? "has-error":"";?>">
					<label for="Provincia">Provincia</label>
					<input type="text" name="Provincia" class="form-control" value="<?php echo isset($Provincia) ? $Provincia:""?>">
					<?php  echo contieneError("Provincia")? "<span id='helpBlock2' class='help-block'>".getError("Provincia")."</span>":"";?>
				</div>
				<div class="form-group  <?php echo contieneError("Club")? "has-error":"";?>">
					<label for="Club">Club</label>
					<input type="text" name="Club" id="Club" class="form-control" value="<?php echo isset($Club) ? $Club:""?>">
					<?php  echo contieneError("Club")? "<span id='helpBlock2' class='help-block'>".getError("Club")."</span>":"";?>
				</div>
				<label>
				      <input type="checkbox" name="ViveEnZizur" value="true" <?php echo (isset($ViveEnZizur) && $ViveEnZizur=="true")? "checked":"" ?>> ¿Vives en Zizur?
			   </label>
				
				<div class="" style="display:none" >
				<!-- <div class="form-group  <?php echo contieneError("TallaCamiseta")? "has-error":"";?>">
					<label for="TallaCamiseta">Talla de Camiseta</label>
					<select name="TallaCamiseta" class="form-control">
						<option value="">---</option>
					 	<option value="S" <?php echo (isset($TallaCamiseta) && $TallaCamiseta=="S") ?  "selected" :""?>>S</option>
						<option value="M" <?php echo (isset($TallaCamiseta) && $TallaCamiseta=="M") ?  "selected" :""?>>M</option>
						<option value="L" <?php echo (isset($TallaCamiseta) && $TallaCamiseta=="L") ?  "selected" :""?>>L</option>
						<option value="XL" <?php echo(isset($TallaCamiseta) && $TallaCamiseta=="XL") ?  "selected" :""?>>XL</option>
					</select> 
					<?php  echo contieneError("TallaCamiseta")? "<span id='helpBlock2' class='help-block'>".getError("TallaCamiseta")."</span>":"";?>
				</div>
				 -->
				</div>
				<div class="form-group">
				<input type="submit" class="btn btn-default" value="Enviar"> 
				</div>
				
			
			</form>
	
	</div>
	
	
	  	</div>
		</div>
	
	
	

	
	


</div>
</body>
</html>