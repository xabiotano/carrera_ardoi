<?php 
	
	require_once 'inscripcion/classes/PagoInscripcion.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>XXIV CARRERA CAMINO DE SANTIAGO -- EDICIÓN 2017 5/10KM</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/noinscripcion.css" rel="stylesheet">
	<link href="css/animate.css" rel="stylesheet">	
	<link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <style>
    
	    body{
	   	 background-color:#19153d;
	   	 color:#000;
	    }
    </style>
</head><!--/head-->

<body>
	<header id="header" role="banner">		
		<div class="main-nav">
			<div class="container">
				
		        <div class="row">	        		
		            <div class="navbar-header">
		                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		                    <span class="sr-only">Toggle navigation</span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
		                <a class="navbar-brand" href="index.php">
		                 <img  src="images/logo.png"/>
		                </a>                    
		            </div>
		            <?php include_once 'componentes/navbar.php';?>
		        </div>
	        </div>
        </div>                    
    </header>
    <!--/#header--> 
    
    <div class="container">
    	<div class="row">
    	<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
			<div class="panel panel-default">
	      		
	 		  	<div class="panel-body">
	 		  	
				
				<div class="col-sm-12 col-md-12">
						 <h2>Lista de Inscritos</h2>
						  <p>Aqui tienes una lista de los inscritos en las distintas categorías</p>  
						  <?php 
						  //obtengo los inscritos
						  $filtro=null;
						  if(isset($_GET["filtro"])){
						  	$filtro=$_GET["filtro"];
						  }else{
						  	$filtro="no";
						  }
						  if($filtro=="no"){
						  	 
						  }
						  
						  
						  ?>          
						  <a class="btn <?php echo $filtro=="no"? "btn-primary":"btn-default"?>" href="inscritos.php" role="button">Todos</a>
						   <a class="btn <?php echo $filtro=="11"? "btn-primary":"btn-default"?>" href="inscritos.php?filtro=11" role="button">5Km</a>
						  <a class="btn <?php echo $filtro=="10"? "btn-primary":"btn-default"?>" href="inscritos.php?filtro=10" role="button">10Km</a>
						  <a class="btn <?php echo $filtro=="0"? "btn-primary":"btn-default"?>" href="inscritos.php?filtro=0" role="button">Categorias menores</a>
						  <br/>
						  <br/>
						  <br/>
						  <table class="table table-condensed">
						    <thead>
						      <tr class="active">
						     	<th>Dorsal</th>
						        <th>Nombre</th>
						        <th>Apellido </th>
						        <th>Apellido 2</th>
						        <th>Club</th>
						        <th>Categoria</th>
						      </tr>
						    </thead>
						    <tbody>
						      
						      <?php 
						     	
						      	$inscritos=PagoInscripcion::GetInscritos($filtro);
						      	$i=1;
								foreach ($inscritos as $k => $v) {
									
									echo "<tr class='active'>";
										echo "<td>".$v->dorsal."</td>";
										echo "<td>".$v->nombre."</td>";
										echo "<td>".$v->apellido1."</td>";
										echo "<td>".$v->apellido2."</td>";
										echo "<td>".$v->club."</td>";
										echo "<td>".$v->categoria."</td>";
										if($v->carrera_eleg==10){
											echo "<td>10 KM</td>";
										}else if($v->carrera_eleg==11){
											echo "<td>5 KM</td>";
										}else if($v->carrera_eleg==0){
											echo "<td>Carreras Infantiles</td>";
										}
									echo "</tr>";
									$i++;
								   
								}
								
							
						      
						      
						      ?>
						      
						      
						    </tbody>
						  </table>
						</div>
		 		  	
		 		  	</div>
		 		</div>
	 	</div>
 		  		
 	</div>
 		
   
	
				
	
	
	
	




	
	
	
	


     <?php include_once 'componentes/footer.php';?>
  
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
  	<script type="text/javascript" src="js/gmaps.js"></script>
	<script type="text/javascript" src="js/smoothscroll.js"></script>
    <script type="text/javascript" src="js/jquery.parallax.js"></script>
    <script type="text/javascript" src="js/coundown-timer.js"></script>
    <script type="text/javascript" src="js/jquery.scrollTo.js"></script>
    <script type="text/javascript" src="js/jquery.nav.js"></script>
    <script type="text/javascript" src="js/main.js"></script>  
</body>
</html>